export const setUserAuth = (userAuths) => {
    return {
        type: 'SET_USER_AUTH',
        userAuths
    }
}
export const setOrgId = (orgId) => {
    return {
        type: 'SET_ORG_ID',
        orgId
    }
}
export const setUserProps = (userName, userId, department) => {
    return {
        type: 'SET_USER_PROPS',
        userName,
        userId,
        department
    }
}
export const setTemplates = (templates) => {
    return {
        type: 'SET_TEMPLATES',
        templates
    }
}
export const deleteTemplate = (template) => {
    return {
        type: 'DELETE_TEMPLATE',
        template
    }
}
export const setQueues = (queues) => {
    return {
        type: 'SET_QUEUES',
        queues
    }
}

export const setSkills = (skills) => {
    return {
        type: 'SET_SKILLS',
        skills
    }
}
export const setUsers = (allUsers) => {
    return {
        type: 'SET_USERS',
        allUsers
    }
}
export const setUserTemplateMap = (userTemplateMap) => {
    return {
        type: 'SET_TEMPLATE_MAP',
        userTemplateMap
    }
}
export const setAssignedTemplate = (assignedTemplate) => {
    return {
        type: 'SET_ASSIGNED_TEMPLATE',
        assignedTemplate
    }
}
export const setAssignedQueue = (assignedQueue) => {
    return {
        type: 'SET_ASSIGNED_QUEUE',
        assignedQueue
    }
}
export const setAssignedSkill = (assignedSkill) => {
    return {
        type: 'SET_ASSIGNED_SKILL',
        assignedSkill
    }
}
export const setFilteredTemplates = (filteredTemplates) => {
    return {
        type: 'SET_FILTERED_TEMPLATES',
        filteredTemplates
    }
}
export const setIsFiltered = (isFiltered) => {
    return {
        type: 'SET_IS_FILTERED',
        isFiltered
    }
}

export const setIsUpdating = (isUpdating) => {
    return {
        type: 'SET_IS_UPDATING',
        isUpdating
    }
}

export const setPureCloud = (isPureCloud) => {
    return {
        type: 'SET_PURECLOUD',
        isPureCloud
    }
}
export const setTemplateRole = (templateRole) => {
    return {
        type: 'SET_TEMPLATE_ROLE',
        templateRole
    }
}
export const setUpdatesPending = (pending) => {
    return {
        type: 'SET_UPDATES_PENDING',
        pending
    }
}