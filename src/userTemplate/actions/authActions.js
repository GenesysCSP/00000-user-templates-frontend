import { createAction } from 'redux-actions';
// import purecloud from '../services/purecloud';
// import { setAppList } from './appActions';

// Redux Action Generators
//const setAuthRegion = createAction('AUTH_SET_REGION');
const setAuthAccessToken = createAction('AUTH_SET_ACCESS_TOKEN');
const setAuthEnvironment = createAction('AUTH_SET_ENVIRONMENT');
const setAuthOrganizationInfo = createAction('AUTH_SET_ORGANIZATION_INFO');
//const setAuthUserRoles = createAction('AUTH_SET_USER_ROLES');
const setAuthError = createAction('AUTH_SET_ERROR', (message, code) => ({ message, code }));
const setAuthCheckingAccess = createAction('AUTH_SET_CHECKING_ACCESS');
//const setAuthAccessGranted = createAction('AUTH_SET_ACCESS_GRANTED');
const clearAuthState = createAction('AUTH_CLEAR_STATE');

// Redux Thunk Generators
// const startLogin = (region) => (dispatch) => {
//   dispatch(setAuthRegion(region));
//   return purecloud.login(region);
// };

// const fetchUserRoles = () => (dispatch) => {
//   return purecloud.fetchUserRoles().then((result) => {
//     if (result.success) {
//       dispatch(setAuthUserRoles(result.roles));
//     } else {
//       dispatch(setAuthUserRoles(result.error));
//     }
//   });
// };

// const determineUserAccess = () => (dispatch, getState) => {
//   const state = getState();
//   let userRoles = state.auth && state.auth.roles;
//   let applications = state.app && state.app.list;
//   if (!userRoles || !applications || userRoles.error || applications.error) {
//     let errorMessage;
//     if (!userRoles || userRoles.error) {
//       errorMessage =
//         'There was an issue getting PureCloud roles for your user account. ' +
//         'Please contact your PureCloud administrator for assistance.';
//     } else {
//       errorMessage =
//         'There was an issue getting an application list for your organization. ' +
//         'Please contact your PureCloud administrator for assistance.';
//     }
//     dispatch(setAuthError(errorMessage));
//     dispatch(setAuthAccessGranted(false));
//     purecloud.logout();
//     return;
//   } else {
//     userRoles = userRoles.value;
//     applications = applications.value;
//   }

//   let accessGranted;
//   applications.forEach((app) => {
//     const accessRoles = app.accessRoles;
//     let canAccessApp = false;
//     if (!accessRoles) {
//       canAccessApp = false;
//     } else {
//       canAccessApp = accessRoles.find((ar) =>
//         userRoles.find((ur) => ar.toLowerCase() === ur.name.toLowerCase())
//       );
//     }
//     if (canAccessApp) {
//       accessGranted = true;
//     }
//     app.accessGranted = !!canAccessApp;
//   });
//   dispatch(setAppList(applications));
//   if (accessGranted) {
//     dispatch(setAuthAccessGranted(true));
//   } else {
//     const errorMessage =
//       'You do not have the proper PureCloud role to login. ' +
//       'Please contact your PureCloud administrator for assistance.';
//     dispatch(setAuthError(errorMessage));
//     dispatch(setAuthAccessGranted(false));
//     purecloud.logout();
//   }
// };

const checkForAuthErrors = (error) => (dispatch) => {
  if (error.response && error.response.status && error.response.status === 401) {
    const errorCode = error.response.status;
    const errorMessage = 'Your PureCloud session is no longer active. Please re-login to continue';
    dispatch(setAuthError(errorMessage, errorCode));
    return true;
  } else {
    return false;
  }
};

// const startLogout = () => (dispatch) => {
//   return purecloud.logout().then(() => {
//     dispatch(clearAuthState());
//   });
// };

export {
  checkForAuthErrors,
  clearAuthState,
  // determineUserAccess,
  // fetchUserRoles,
  setAuthAccessToken,
  setAuthCheckingAccess,
  setAuthEnvironment,
  setAuthError,
  setAuthOrganizationInfo,
  // startLogin,
  // startLogout,
};
