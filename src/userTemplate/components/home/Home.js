import './Home.css'

import React from 'react'
import { Button, Alert } from 'reactstrap'
import Header from '../header/Header';
import SupervisorTemplate from '../supervisorTemplate/SupervisorTemplate'
import AgentTemplate from '../AgentTemplate/AgentTemplate'
import UserTemplate from '../userTemplate/UserTemplate'
import { connect } from 'react-redux';
import { config, userRole, agentRole, supervisorRole } from '../../../config/config'
import { setUserAuth, setTemplates, setUsers, setUserTemplateMap, setAssignedTemplate, setFilteredTemplates, setIsFiltered, setAssignedQueue, setAssignedSkill } from '../../actions/userActions'
import { fetchApplicationConfig, fetchUserTemplateMapping } from '../../services/aws';
import { getAllUsers, getAllRoleUsers } from '../../services/purecloud'
import utils, { isObjectEmpty } from '../../utilities/utils';
import WaitingModal from '../Modals/WaitingModal/WaitingModal'
import moment from 'moment';

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isSupervisor: false,
            isAgent: false,
            isUser: false,
            loadingConfig: true,
            loadingError: '',
            timerId: '',
            updatesPending: false
        }

        if (this.props.userAuths.filter(s => s.includes(supervisorRole)).length > 0) {
            this.state = { isSupervisor: true }
        } else if (this.props.userAuths.filter(s => s.includes(agentRole)).length > 0 && this.props.userAuths.filter(s => s.includes(userRole)).length > 0) {
            this.state = { isAgent: true }
        } else if (this.props.userAuths.filter(s => s.includes(userRole)).length > 0) {
            this.state = { isUser: true }
        }

    }

    loadingError = 'There was an error loading the configuration. Please refresh ' +
        'your browser to try again. If this message persists, please contact your ' +
        'your support personnel for assistance.';

    userId = this.props.userId

    componentDidMount() {
        if (this.state.isUser || this.state.isAgent || this.state.isSupervisor) {
            localStorage.removeItem("unauthCount");
            this.setState(() => ({ loadingConfig: true, updatesPending: false }));
            this.getConfigurations().then((result) => {
                if (result && result.success) {
                    const latestTemplateMap = result.templateMap.filter(x => x.lastAppliedDate != null);
                    latestTemplateMap?.sort((a, b) => {
                        return b.lastAppliedDate?.localeCompare(a.lastAppliedDate);
                    })
                    this.props.setTemplates(result.templates)
                    this.props.setUserTemplateMap(result.templateMap)
                    this.props.setUsers(result.users)
                    let templateDetails = latestTemplateMap.find(x => x.userId.filter(y => y === this.props.userId)[0])
                    const templateName = templateDetails?.templateName;
                    let assignedTemplate = templateName ? templateName : "No Template Assigned";
                    this.props.setAssignedTemplate(assignedTemplate)
                    if (result.filteredTemplates.length > 0) {
                        this.props.setFilteredTemplates(result.filteredTemplates)
                        this.props.setIsFiltered(true)
                    }
                    if (assignedTemplate !== "No Template Assigned") {
                        this.props.setAssignedQueue(result.templates.find(x => x.displayName === assignedTemplate)?.queueList.map(x => x.name).join(","))
                        this.props.setAssignedSkill(result.templates.find(x => x.displayName === assignedTemplate)?.skillList.map(x => x.name).join(","))
                    }
                }
                else {
                    console.log(result.message)
                    this.setState(() => ({ loadingConfig: false }));
                    this.setState(() => ({ loadingError: this.loadingError }));
                }
            })
        } else {
            this.setState(() => ({ loadingConfig: false, updatesPending: false }));
            let count = localStorage.getItem("unauthCount") ? 2 : 0;
            if (count <= 1) {
                localStorage.setItem("unauthCount", 1);
                sessionStorage.clear()
                this.props.history.push('/login')
            }
            console.log("Not authorized to use app");
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.timerId)
    }

    pollForUpdates = () => {
        if (this.props.isUpdating) {
            console.log('Save is in progress. Cancelling update')
            return
        }

        this.getConfigurations().then((result) => {
            if (result && result.success) {
                let templates = result.templates.map((template) => ({
                    value: template.displayName,
                    label: template.displayName,
                    ...template
                }))
                if (JSON.stringify(templates) !== JSON.stringify(this.props.templates)) {
                    this.setState(() => ({ updatesPending: true }));
                    clearInterval(this.state.timerId)
                }
                else if (JSON.stringify(result.users) !== JSON.stringify(this.props.allUsers)) {
                    this.setState(() => ({ updatesPending: true }));
                    clearInterval(this.state.timerId)
                }
                else if (JSON.stringify(result.templateMap) !== JSON.stringify(this.props.userTemplateMap)) {
                    this.setState(() => ({ updatesPending: true }));
                    clearInterval(this.state.timerId)
                }
                else {
                }
            }
            else {

            }
        })
    }

    getConfigurations = () => {
        return new Promise(resolve => {
            let returnValue = {
                users: [],
                templates: [],
                templateMap: [],
                assignedTemplate: '',
                filteredTemplates: [],
                success: true,
                message: ''
            }
            const promises = [
                fetchApplicationConfig(
                    `${config.backendEndpoint}templates`,
                    utils.accessToken(),
                    utils.getEnv()
                ),
                getAllUsers(
                    utils.accessToken(),
                    utils.getEnv()
                ),
                fetchUserTemplateMapping(
                    `${config.backendEndpoint}template/schedules`,
                    utils.accessToken(),
                    utils.getEnv()
                )
            ];
            if (this.state.isSupervisor) {
                promises.push(
                    getAllRoleUsers(
                        utils.accessToken(),
                        utils.getEnv(),
                        this.props.templateRole
                    ))
            }
            Promise.all(promises)
                .then((result) => {
                    const templateConfig = result[0];
                    const appConfig = templateConfig.config.Item;
                    if (templateConfig.success && !isObjectEmpty(appConfig)) {
                        returnValue.templates = appConfig.templateList
                        //Hari
                        // if (this.props.department) {
                        //     returnValue.filteredTemplates = appConfig.templateList.filter(template => template.displayName.includes(this.props.department))
                        // }
                    }

                    const userEntities = result[1];
                    const allUsers = userEntities.map((entity) => ({
                        text: entity.name,
                        name: entity.name,
                        id: entity.id
                    }));

                    const mappingConfig = result[2];
                    if (mappingConfig.success) {
                        returnValue.templateMap = mappingConfig.message;
                        //TODO
                        let userMap = mappingConfig.message.find(tempMap => { return tempMap.userId === this.userId })
                        if (userMap) {
                            returnValue.assignedTemplate = userMap.template
                        }
                        else {
                            //this.props.setAssignedTemplate('No Template Assigned')
                        }
                    }
                    if (this.state.isSupervisor) {
                        const userRoleEntities = result[3];
                        let roleUsers = []
                        for (var user in userRoleEntities) {
                            let roleUser = allUsers.find(userToCheck => { return userToCheck.id === userRoleEntities[user].id })
                            if (roleUser) {
                                roleUsers.push(roleUser)
                            }
                        }
                        returnValue.users = roleUsers
                    } else {
                        returnValue.users = allUsers
                    }
                    this.setState(() => ({ loadingConfig: false }));
                    resolve(returnValue)
                })
                .catch((error) => {
                    returnValue.success = false
                    returnValue.message = error
                    console.log("getConfigurations" + JSON.stringify(error))
                    this.setState(() => ({ loadingConfig: false }));
                    resolve(returnValue)
                });
        })
    }

    updateConfigurations = () => {
        this.setState(() => ({ updatesPending: false, loadingConfig: true }));
        this.getConfigurations().then((result) => {
            if (result && result.success) {
                this.props.setTemplates(result.templates)
                this.props.setUserTemplateMap(result.templateMap)
                this.props.setUsers(result.users)
                this.props.setAssignedTemplate(result.assignedTemplate)
                //Hari
                // if (result.filteredTemplates.length > 0) {
                //     this.props.setFilteredTemplates(result.filteredTemplates)
                //     this.props.setIsFiltered(true)
                // }
                this.setState(() => ({ loadingConfig: false }));
            }
            else {
                console.log(result.message)
                this.setState(() => ({ loadingConfig: false }));
                this.setState(() => ({ loadingError: this.loadingError }));
            }
        })
        // var timerId = setInterval(this.pollForUpdates, 60000)
        // this.setState(() => ({ timerId }));
    }

    dismissUpdate = () => {
        this.setState(() => ({ updatesPending: false }));
        var timerId = setInterval(this.pollForUpdates, 60000)
        this.setState(() => ({ timerId }));
    }

    render() {
        return (
            <div>
                <Header history={this.props.history} />
                <WaitingModal isOpen={this.state.loadingConfig} header="Loading Configuration" />
                <Alert color="warning" isOpen={this.state.updatesPending} toggle={this.dismissUpdate}>
                    Updated configurations available.
                    <Button
                        className="float-right"
                        color="warning"
                        onClick={this.updateConfigurations}
                        size="sm">
                        Refresh.
                    </Button>
                </Alert>
                <div>
                    {this.state.isSupervisor && (
                        <SupervisorTemplate></SupervisorTemplate>
                    )}
                    {this.state.isAgent && (
                        <AgentTemplate></AgentTemplate>
                    )}
                    {this.state.isUser && (
                        <UserTemplate></UserTemplate>
                    )}
                    {!this.state.isUser && !this.state.isAgent && !this.state.isSupervisor && (
                        "You are not authorized to view this page."
                    )}
                </div>
                {/*Footer*/}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userAuths: state.userAuths,
    department: state.department,
    userName: state.userName,
    userId: state.userId,
    orgId: state.orgId,
    templates: state.templates,
    isPureCloud: state.isPureCloud,
    allUsers: state.allUsers,
    userTemplateMap: state.userTemplateMap,
    templateRole: state.templateRole,
    isUpdating: state.isUpdating
});

const mapDispatchToProps = (dispatch) => ({
    setTemplates: (templates) => dispatch(setTemplates(templates)),
    setUserAuth: (userAuths) => dispatch(setUserAuth(userAuths)),
    setUsers: (allUsers) => dispatch(setUsers(allUsers)),
    setUserTemplateMap: (userTemplateMap) => dispatch(setUserTemplateMap(userTemplateMap)),
    setAssignedTemplate: (assignedTemplate) => dispatch(setAssignedTemplate(assignedTemplate)),
    setAssignedQueue: (assignedQueue) => dispatch(setAssignedQueue(assignedQueue)),
    setAssignedSkill: (assignedSkill) => dispatch(setAssignedSkill(assignedSkill)),
    setFilteredTemplates: (filteredTemplates) => dispatch(setFilteredTemplates(filteredTemplates)),
    setIsFiltered: (isFiltered) => dispatch(setIsFiltered(isFiltered))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home)