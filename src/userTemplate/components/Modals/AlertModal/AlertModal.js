import React from 'react';
import { Alert, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import PropTypes from 'prop-types';
import './AlertModal.css';

const AlertModal = ({
  isOpen,
  toggle,
  header,
  body,
  color = 'danger',
  buttonText = 'Close',
  onButtonClick,
}) => (
  <Modal className="alert-modal animated fadeIn" isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>{header}</ModalHeader>
    <ModalBody>
      <Alert color={color}>{body}</Alert>
    </ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={onButtonClick}>
        {buttonText}
      </Button>
    </ModalFooter>
  </Modal>
);

AlertModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  color: PropTypes.string,
  buttonText: PropTypes.string,
  onButtonClick: PropTypes.func.isRequired,
};

export default AlertModal;
