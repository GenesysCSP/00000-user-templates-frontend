import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import PropTypes from 'prop-types';
import './MessageModal.css';

const MessageModal = ({ isOpen, toggle, header, body, buttonText = 'Close', onButtonClick }) => (
  <Modal className="message-modal animated fadeIn" isOpen={isOpen} toggle={toggle}>
    <ModalHeader toggle={toggle}>{header}</ModalHeader>
    <ModalBody>{body}</ModalBody>
    <ModalFooter>
      <Button color="primary" onClick={onButtonClick}>
        {buttonText}
      </Button>
    </ModalFooter>
  </Modal>
);

MessageModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  header: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  buttonText: PropTypes.string,
  toggle: PropTypes.func.isRequired,
  onButtonClick: PropTypes.func.isRequired,
};

export default MessageModal;
