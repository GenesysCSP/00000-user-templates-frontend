import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { BeatLoader } from 'react-spinners';
import PropTypes from 'prop-types';
import './WaitingModal.css';

const WaitingModal = ({ isOpen, header = 'Loading' }) => (
  <Modal className="waiting-modal waiting-modal--centered animated fadeIn" isOpen={isOpen}>
    <ModalHeader className="waiting-modal--centered">{header}</ModalHeader>
    <ModalBody className="waiting-modal--centered">
      <div className="waiting-modal__spinner">
        <BeatLoader color="currentColor" />
      </div>
    </ModalBody>
  </Modal>
);

WaitingModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  header: PropTypes.string,
};

export default WaitingModal;
