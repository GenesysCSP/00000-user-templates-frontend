import './PureCloudLogin.css'

import React from 'react'
import { Container, Row, Col, Input, Button, Form, InputGroup, InputGroupAddon, InputGroupButtonDropdown, DropdownMenu, DropdownItem } from 'reactstrap'
import { getClientId, getEnvironments } from '../../services/purecloud'
import ReactSVG from 'react-svg'

class PureCloudLogin extends React.Component {
    constructor(props) {
        super(props)
        const environments = getEnvironments()
        this.state = {
            environments: environments,
            selectedEnvironment: environments[0],
            splitButtonOpen: false
        }
    }
    handleEnvironmentChanged = (e) => {
        const selectedEnvironment = e.target.value
        this.setState({ selectedEnvironment })
    }
    onSubmit = (e) => {
        sessionStorage.setItem('purecloud-csp-env', this.state.selectedEnvironment)
        e.preventDefault()
        const queryStringData = {
            response_type: 'token',
            client_id: getClientId(this.state.selectedEnvironment),
            redirect_uri: `${window.location.protocol}//${window.location.host}/`
        }
        const param = Object.keys(queryStringData).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(queryStringData[k])}`).join('&')
        window.location = `https://login.` + this.state.selectedEnvironment + `/oauth/authorize?${param}`
    }
    toggleSplit = () => {
        this.setState({ splitButtonOpen: !this.state.splitButtonOpen })
    }
    render() {
        return (
            <div className="purecloud-login-container">
                <Container className="purecloud-login-vert-center">
                    <Row>
                        <Col>
                            <ReactSVG path="./resources/purecloud/purecloud-color-text.svg" className="purecloud-login-svg" />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form onSubmit={this.onSubmit}>
                                <InputGroup className="purecloud-login-input-group">
                                    <InputGroupButtonDropdown addonType="prepend" isOpen={this.state.splitButtonOpen} toggle={this.toggleSplit}>
                                        <DropdownMenu>
                                            <DropdownItem header>Header</DropdownItem>
                                            <DropdownItem disabled>Action</DropdownItem>
                                            <DropdownItem>Another Action</DropdownItem>
                                            <DropdownItem divider />
                                            <DropdownItem>Another Action</DropdownItem>
                                        </DropdownMenu>
                                    </InputGroupButtonDropdown>
                                    <Input type="select" id="exampleEmail" onChange={this.handleEnvironmentChanged} className="purecloud-login-form-control">
                                        {
                                            this.state.environments &&
                                            this.state.environments.map(e => (<option key={e} value={e}>{e}</option>))
                                        }
                                    </Input>
                                    <InputGroupAddon addonType="append"><Button color="secondary">Log In</Button></InputGroupAddon>
                                </InputGroup>
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default PureCloudLogin