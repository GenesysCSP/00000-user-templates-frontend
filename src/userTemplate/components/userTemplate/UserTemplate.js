import React, { Component } from 'react';
import { Label } from 'reactstrap';
import Container from 'react-bootstrap/Container'
import './UserTemplate.css';
import { connect } from 'react-redux';
import { setUserTemplateMap, setAssignedTemplate, setIsFiltered, setAssignedQueue, setAssignedSkill } from '../../actions/userActions'

export class UserTemplate extends Component {

  state = {
    showHelp: false
  };

  colorGenesysOrange = '#f15833';

  handleTemplateSelectionChange = (selectedTemplate) => {
    let selectedQueues = '';
    let selectedSkills = '';
    for (var queue in selectedTemplate.queueList) {
      selectedQueues = selectedQueues ? selectedQueues + ", " + selectedTemplate.queueList[queue].name : selectedTemplate.queueList[queue].name
    }
    for (var skill in selectedTemplate.skillList) {
      selectedSkills = selectedSkills ? selectedSkills + ", " + selectedTemplate.skillList[skill].name : selectedTemplate.skillList[skill].name
    }
    this.setState(() => ({
      selectedTemplate,
      selectedQueues,
      selectedSkills
    }));
  }

  render() {
    return (
      <div className="user-template-user-tab">
        <Container>
          <div className="user-template-user-tab__label">
            <Label>
              Current Assigned Template: {this.props.assignedTemplate}
            </Label>
            <hr></hr>
          </div>
          <div className="user-template-user-tab__input">
            <Label>
              Queues:
            </Label>
            {' ' + this.props.assignedQueue}
          </div>
          <br />
          <div className="user-template-user-tab__input">
            <Label>
              Skills:
            </Label>
            {' ' + this.props.assignedSkill}
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  templates: state.templates,
  userId: state.userId,
  orgId: state.orgId,
  allUsers: state.allUsers,
  userTemplateMap: state.userTemplateMap,
  assignedTemplate: state.assignedTemplate,
  assignedQueue: state.assignedQueue,
  assignedSkill: state.assignedSkill,
  isFiltered: state.isFiltered,
  filteredTemplates: state.filteredTemplates
});

const mapDispatchToProps = (dispatch) => ({
  setUserTemplateMap: (userTemplateMap) => dispatch(setUserTemplateMap(userTemplateMap)),
  setAssignedTemplate: (assignedTemplate) => dispatch(setAssignedTemplate(assignedTemplate)),
  setAssignedQueue: (assignedQueue) => dispatch(setAssignedQueue(assignedQueue)),
  setAssignedSkill: (assignedSkill) => dispatch(setAssignedSkill(assignedSkill)),
  setIsFiltered: (isFiltered) => dispatch(setIsFiltered(isFiltered))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTemplate)
