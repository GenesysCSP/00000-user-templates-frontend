import React, { Component } from 'react';
import { Fade, Label, Button } from 'reactstrap';
import Select from 'react-select';
import Ionicon from 'react-ionicons';
import classNames from 'classnames';
import { Alert } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import './AgentTemplate.css';
import { connect } from 'react-redux';
import { config } from '../../../config/config'
import { addScheduledTemplates } from '../../services/aws';
import { setUserTemplateMap, setAssignedTemplate, setIsFiltered } from '../../actions/userActions'
import utils, { compareValues } from '../../utilities/utils'
import MessageModal from '../Modals/MessageModal/MessageModal';
import AlertModal from '../Modals/AlertModal/AlertModal';
import WaitingModal from '../Modals/WaitingModal/WaitingModal'
import { addRemoveQueues, addSkills, getAllQueues, setGCUserTemplateMappings } from '../../services/purecloud'
import moment from 'moment';


export class UserTemplate extends Component {
  saveError = 'There was an error saving the configuration. Please try again. ' +
    'If this message persists, please contact your support personnel for assistance.';
  state = {
    saveError: '',
    savingConfig: false,
    showSaveConfirmation: false,
    showHelp: false,
    selectedTemplate: null,
    assignedTemplate: '',
    selectedQueues: '',
    selectedSkills: ''
  };
  colorGenesysOrange = '#f15833';
  helpMessage = 'Select which template to apply. Dont refresh the browser or close the page while update is in progress.';

  handleTemplateSelectionChange = (selectedTemplate) => {
    let selectedQueues = '';
    let selectedSkills = '';
    for (var queue in selectedTemplate?.queueList) {
      selectedQueues = selectedQueues ? selectedQueues + ", " + selectedTemplate?.queueList[queue].name : selectedTemplate?.queueList[queue].name
    }
    for (var skill in selectedTemplate?.skillList) {
      selectedSkills = selectedSkills ? selectedSkills + ", " + selectedTemplate?.skillList[skill].name : selectedTemplate?.skillList[skill].name
    }
    this.setState(() => ({
      selectedTemplate,
      selectedQueues,
      selectedSkills
    }));
  }

  initialTemplateLoad = (templateName) => {
    if (templateName != "No Template Assigned") {
      this.handleTemplateSelectionChange(this.props.templates.find(x => x.displayName === this.props.assignedTemplate));
    }
  }

  selfAssignTemplate = () => {
    let returnValue = {
      success: true,
      message: ''
    }
    return new Promise(resolve => {
      const promise = [this.removeAddQueues(), this.removeAddSkills()];

      Promise.all(promise).then((obj) => {
        returnValue.success = true;
        returnValue.message = obj;
        resolve(returnValue);
      }).catch(err => {
        returnValue.success = false;
        returnValue.message = err;
        console.log("Save user template" + JSON.stringify(err))
        this.setState(() => ({ savingConfig: false }));
        resolve(returnValue)
      })
    })
  }

  async removeAddQueues() {
    const removeQueue = await getAllQueues([this.props.userId]);
    const isAddOrRemove = [true, false];
    for (let i = 0; i < isAddOrRemove.length; i++) {
      let queue = isAddOrRemove[i] ? removeQueue : this.state.selectedTemplate?.queueList.map(x => x.id);
      await addRemoveQueues([[this.props.userId]], isAddOrRemove[i], queue);
    }
    return true;
  }

  async removeAddSkills() {
    let requestType = 'PUT';
    let saveSkill = utils.handleSkillsList(this.state.selectedTemplate.skillList);
    for (let i = 0; i < saveSkill.length; i++) {
      const res = await addSkills([this.props.userId], [saveSkill[i]], requestType);
      requestType = 'PATCH';
    }
    return true;
  }

  handleSaveButtonClick = async () => {
    this.setState(() => ({ savingConfig: true }));

    let gcResponse = await this.selfAssignTemplate();

    if (gcResponse) {
      const isUserExisit = [];

      if (this.props.userTemplateMap.find(x => x.templateName === this.state.selectedTemplate.displayName)) {
        this.isUserExisit = this.props.userTemplateMap.find(x => x.templateName === this.state.selectedTemplate.displayName)[0]?.userId;
      } else {
        this.isUserExisit = this.props.templates.find(x => x.displayName === this.state.selectedTemplate.displayName)[0]?.userId;
      }

      let requestBody = {
        lastAppliedDate: moment().utc().format(),
        orgId: this.props.orgId,
        scheduleDetails: isUserExisit.length > 0 ? this.props.userTemplateMap.find(x => x.templateName === this.state.selectedTemplate.displayName)[0]?.scheduleDetails : {},
        templateName: this.state.selectedTemplate.displayName,
        userId: isUserExisit?.find(x => x !== this.props.userId) ?
          [...this.props.userTemplateMap.find(x => x.templateName === this.state.selectedTemplate.displayName)[0]?.userId, this.props.userId] :
          [this.props.userId]
      }
      let response = await addScheduledTemplates(
        `${config.backendEndpoint}template/schedules`,
        utils.accessToken(),
        utils.getEnv(),
        requestBody
      )

      if (response && response.success) {
        this.toggleSaveConfirmation();
        this.props.setAssignedTemplate(this.state.selectedTemplate.displayName)
      } else {
        this.setState(() => ({ saveError: this.saveError }));
      }
    }
    this.setState(() => ({ savingConfig: false }));
  }

  toggleHelp = () => {
    this.setState(() => ({ showHelp: !this.state.showHelp }));
  };

  toggleSaveConfirmation = () => {
    this.setState((prevState) => ({ showSaveConfirmation: !prevState.showSaveConfirmation }));
  };

  clearSaveError = () => {
    this.setState(() => ({ saveError: '' }));
  };
  render() {
    return (
      <div className="user-template-user-tab">
        <Container>
          <WaitingModal isOpen={this.state.savingConfig} header="Saving Configuration" />
          <MessageModal
            isOpen={this.state.showSaveConfirmation}
            toggle={this.toggleSaveConfirmation}
            header="Save Confirmation"
            body="Your configuration changes were saved successfully"
            onButtonClick={this.toggleSaveConfirmation}
          />
          <AlertModal
            isOpen={!!this.state.saveError}
            toggle={this.clearSaveError}
            header="Error Saving Configuration"
            body={this.state.saveError}
            onButtonClick={this.clearSaveError}
          />
          <div
            className={classNames({
              'user-template-user-tab__help': true,
              'user-template-user-tab__help--open': this.state.showHelp,
            })}
          >
            {this.state.showHelp && (
              <Fade in={this.state.showHelp}>
                <div>
                  <Alert variant="success">{this.helpMessage}
                  </Alert>
                </div>
              </Fade>
            )}
            <Ionicon
              className="user-template-user-tab__help-icon"
              color={this.colorGenesysOrange}
              fontSize="1.5rem"
              icon="md-help-circle"
              onClick={this.toggleHelp}
            />
          </div>
          <div className="user-template-user-tab__input">
            <Label>
              Select a New Template
            </Label>
            <Select
              className="user-template-select"
              placeholder="Type to filter..."
              value={this.state.selectedTemplate || this.initialTemplateLoad(this.props.assignedTemplate)}
              onChange={this.handleTemplateSelectionChange}
              options={this.props.isFiltered ? this.props.filteredTemplates.sort(compareValues('displayName')) : this.props.templates.sort(compareValues('displayName'))}
            />
          </div>
          <br />
          <div className="user-template-user-tab__input">
            <Label>
              Queues:
            </Label>
            {' ' + this.state.selectedQueues}
          </div>
          <br />
          <div className="user-template-user-tab__input">
            <Label>
              Skills:
            </Label>
            {' ' + this.state.selectedSkills}
          </div>
          <div className="user-template-user-tab__save-button">
            <Button
              color="success"
              onClick={this.handleSaveButtonClick}
              disabled={!this.state.selectedTemplate}>
              Assign Template
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  templates: state.templates,
  userId: state.userId,
  orgId: state.orgId,
  allUsers: state.allUsers,
  userTemplateMap: state.userTemplateMap,
  assignedTemplate: state.assignedTemplate,
  isFiltered: state.isFiltered,
  filteredTemplates: state.filteredTemplates
});

const mapDispatchToProps = (dispatch) => ({
  setUserTemplateMap: (userTemplateMap) => dispatch(setUserTemplateMap(userTemplateMap)),
  setAssignedTemplate: (assignedTemplate) => dispatch(setAssignedTemplate(assignedTemplate)),
  setIsFiltered: (isFiltered) => dispatch(setIsFiltered(isFiltered))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTemplate)
