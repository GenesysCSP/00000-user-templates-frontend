import React from 'react'
import utils, { getParameterByName } from '../utilities/utils';
import { getOrgId, getMe, getAuth, getClientId, getAllRoles, getOrgConfig } from '../services/purecloud';
import { getAuthorization } from '../services/aws';
import { config, userRole } from '../../config/config'
import { setUserAuth, setOrgId, setUserProps, setPureCloud, setTemplateRole } from '../actions/userActions'
import { connect } from 'react-redux';
/*
    DO NOT EDIT THIS PAGE.
*/
class Main extends React.Component {
    user = ""
    constructor(props) {
        super(props)
        if (getParameterByName('access_token') || utils.accessToken()) {
            if (getParameterByName('access_token'))
                sessionStorage.setItem('purecloud-csp-token', getParameterByName('access_token'))

            let token = utils.accessToken()
            let environment = utils.getEnv()
            if (environment) {
                this.props.setPureCloud(true)
            }


            getOrgId(token, environment).then(async response => {
                if (response.status === 401) {
                    sessionStorage.clear();
                    console.log('redirecting to login')
                    this.props.history.push('/login')
                    return false
                }
                const pageSize = await getOrgConfig(
                    `${config.backendEndpoint}getOrgConfig`,
                    utils.accessToken(),
                    utils.getEnv()
                );
                config.pageSize = pageSize.message?.pageSize ? pageSize.message?.pageSize : 100;
                this.props.setOrgId(response.id);
                sessionStorage.setItem('orgId', response.id)
                sessionStorage.setItem(`purecloud-csp-token-${response.id}`, token)
                sessionStorage.setItem(`purecloud-csp-env-${response.id}`, environment)
                sessionStorage.removeItem('purecloud-csp-token');
                sessionStorage.removeItem('purecloud-csp-env');
                return getMe(token, environment)
            }).then(response => {
                this.user = response
                this.props.setUserProps(response.name, response.id, response.department)
                return getAuth(token, environment)
            }).then(response => {
                this.props.setUserAuth(response.grants.map((grant) => (grant.role.name)))
                return getAllRoles(token, environment)
            }).then(response => {
                let templateRole = response.find(role => { return role.name === userRole })
                if (templateRole) {
                    this.props.setTemplateRole(templateRole.id)
                }
                else {
                    console.log(userRole, ' role not found')
                }
                return getAuthorization(config.backendEndpoint, token, environment)
            }).then(response => {
                console.log(response)
                if (response.customerVersion === "AF") {
                    const hasAFPermission = this.user.authorization.permissionPolicies.find(p => p.domain === 'integration' && p.entityName === config.permissionPolicyEntityName) ? true : false

                    if (!hasAFPermission)
                        this.props.setUserAuth([])
                }
                this.props.history.push('/dashboard')
            }).catch(error => {
                console.log(error)
                this.props.setUserAuth([])
                this.props.history.push('/dashboard');
            })
        } else if (getParameterByName('environment')) {
            sessionStorage.setItem('purecloud-csp-env', getParameterByName('environment'))
            this.props.setPureCloud(true)
            const queryStringData = {
                response_type: 'token',
                client_id: getClientId(getParameterByName('environment')),
                redirect_uri: `${window.location.protocol}//${window.location.host}/main`
            }
            const param = Object.keys(queryStringData).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(queryStringData[k])}`).join('&')
            window.location = `https://login.` + getParameterByName('environment') + `/oauth/authorize?${param}`
        } else {
            sessionStorage.clear();
            console.log('redirecting to login')
            this.props.history.push('/login')
        }
    }
    isAuthorized = (appAuthSettings) => {
        if (!appAuthSettings) {
            return false
        }
        let validDate = new Date(appAuthSettings.validThru)
        let now = new Date();
        if (now > validDate) {
            return false
        }
        return true
    }
    render() {
        return (
            <div>
                Loading
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userAuths: state.userAuths,
    department: state.department,
    userName: state.userName,
    userId: state.userId,
    orgId: state.orgId
});

const mapDispatchToProps = (dispatch) => ({
    setUserAuth: (userAuths) => dispatch(setUserAuth(userAuths)),
    setOrgId: (orgId) => dispatch(setOrgId(orgId)),
    setUserProps: (userName, userId, department) => dispatch(setUserProps(userName, userId, department)),
    setPureCloud: (isPureCloud) => dispatch(setPureCloud(isPureCloud)),
    setTemplateRole: (templateRole) => dispatch(setTemplateRole(templateRole))
});

export default connect(mapStateToProps, mapDispatchToProps)(Main)