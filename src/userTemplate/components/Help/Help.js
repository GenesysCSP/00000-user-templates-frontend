import './Help.css'

import React from 'react'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Container from 'react-bootstrap/Container'
import Image from 'react-bootstrap/Image'
import Header from '../header/Header'

class Help extends React.Component {
    render() {
        return (
            <div>
                <Header history={this.props.history} />
                <Container>
                    <Accordion defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="0">
                                Create Template
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Card.Text>
                                        <strong>Create Template</strong> menu lands in the following page.Enter the name of the template in the textbox and select the required <strong>Queues</strong> and <strong>Skills</strong> and click <strong>Save New Template</strong> button to save the template.Template name can be a maximum of 20 characters and can support only .-_ special characters. Other special characters will not be supported.<br /><br />
                                        To guarantee optimal performance, we have limited the number of queues and skills that can be assigned to a Template. The restrictions are,
                                        <ul style={{ margin: '10px 35px' }}>
                                            <li>500 maximum queues</li>
                                            <li style={{ marginTop: '5px' }}>150 maximum skills</li>
                                        </ul>
                                        <Image src="./images/Create_Template.PNG" fluid />

                                    </Card.Text>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Edit Template
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <Card.Text>
                                        <strong>Edit Template</strong> menu lands in the following page.Select the name of the template from the dropdown and the corresponding selected <strong>Queues and Skills</strong> are shown below. Update and click <strong>Save Changes</strong> button to save the template.<br /><br />
                                        Click <strong>Delete Template</strong> button to delete the template.<br /><br />
                                        To guarantee optimal performance, we have limited the number of queues and skills that can be assigned to a Template. The restrictions are, <br />
                                        <ul style={{ margin: '10px 35px' }}>
                                            <li>500 maximum queues</li>
                                            <li style={{ marginTop: '5px' }}>150 maximum skills</li>
                                        </ul>
                                        <Image src="./images/Edit_Template.PNG" fluid />
                                    </Card.Text></Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="2">
                                Apply Template
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="2">
                                <Card.Body>
                                    <Card.Text>
                                        On Applying a new template, the queues and skills associated with the previous template will be <strong>removed</strong> from the user.<br /><br />
                                        Select <strong>Apply Template</strong> menu for assigning users to the Template. Select the required template from the dropdown. The users with the role <strong>Skill Template User</strong> are listed under the <strong>Available Agents</strong> menu. Select the users and click Add to move them to the <strong>Assigned Agents</strong>. <br />
                                        Click <strong>Apply Changes</strong> to assign the template to the selected users.<br />
                                        The process time depends on the number of users and queues and skills to be assigned.<br /><br />
                                        To guarantee optimal performance, limit the number of agents that can be assigned to a Template. The restrictions are, <br />
                                        <ul style={{ margin: '15px 35px' }}>
                                            <li style={{ marginTop: '5px' }}>500 maximum agents</li>
                                        </ul>
                                        <Image src="./images/Apply_Template.PNG" fluid />
                                    </Card.Text>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="3">
                                Scheduled Template
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="3">
                                <Card.Body>
                                    <Card.Text>
                                        Templates can also be scheduled to run at a specific time. The schedule can be both recursive and non-recursive. The template will be assigned to the agents at the scheduled time.
                                        <br /><br />
                                        For Scheduled Templates, restrict template assignments to
                                        <ul style={{ margin: '10px 35px' }}>
                                            <li>100 maximum queues</li>
                                            <li style={{ marginTop: '5px' }}>100 maximum skills</li>
                                            <li style={{ marginTop: '5px' }}>100 maximum agents</li>
                                        </ul>
                                        <br />
                                        Select <strong>User Template</strong> menu for assigning users to the Template. Select the required template from the dropdown. The users with the role <strong>Skill Template User</strong> are listed under the <strong>Available Agents</strong> menu. Select the users and click Add to move them to the <strong>Assigned Agents</strong>. <br />
                                        Turn on <strong>schedule</strong> button to schedule the template to the selected users.<br /><br />
                                        <strong>Non Recursive Schedule: </strong><br />
                                        Select the Date and time of the schedule. Click on <strong>Save </strong>. The template will run at the scheduled time <br />
                                        <Image src="./images/Non_Recursive_Schedule.PNG" fluid /><br /><br />
                                        <strong>Recursive Schedule: </strong><br />
                                        To make the schedule recurring, turn on the <strong>Recursive </strong> button. Select the type of recursion(Daily, Weekly, Monthly). Click on <strong>Save </strong>. The template will run at the scheduled time <br />
                                        <Image src="./images/Recursive_Schedule.PNG" fluid />
                                    </Card.Text>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="4">
                                Agent View
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="4">
                                <Card.Body>
                                    <Card.Text>
                                        The Agent View will list all the available templates. On selecting a template from the drop down, the <strong>Queues and Skills</strong> mapped to the template will be shown. The agent users should be assigned with both the User Template User and User Template Agent role. <br /><br />
                                        Click <strong>Assign Template</strong> to assign the template.
                                        <Image src="./images/Agent_Template.PNG" fluid />
                                    </Card.Text></Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="5">
                                User View
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="5">
                                <Card.Body>
                                    <Card.Text>
                                        The page shows the details of the template assigned to the user.
                                        <Image src="./images/User_Template.PNG" fluid />
                                    </Card.Text></Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>


                </Container>
            </div>
        )
    }
}

export default Help;