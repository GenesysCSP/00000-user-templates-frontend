import './releaseUpdate.css'

import React from 'react'
import Accordion from 'react-bootstrap/Accordion'
import Card from 'react-bootstrap/Card'
import Container from 'react-bootstrap/Container'
import Image from 'react-bootstrap/Image'
import Header from '../header/Header'
import { Grid } from '@material-ui/core'

class ReleaseUpdate extends React.Component {
    render() {
        return (
            <div>
                <Header history={this.props.history} />
                <Container>
                    <Accordion defaultActiveKey="2">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="2">
                                Jun 12, 2023 - Version 2.4
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="2">
                                <Card.Body>
                                    <Card.Text>
                                        <ul>
                                            <li><h6>The API now includes the ability to configure the pageSize when calling the API, Which is particularly useful for organizations with large Users, Queues and skills.</h6></li>
                                            <li><h6>To address cache issues, we have implemented the webpack.</h6></li>
                                            <br />
                                        </ul>
                                    </Card.Text>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                March 20, 2023 - Version 2.3
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <Card.Text>
                                        <ul>
                                            <li><h6>Enhanced User Templates to address GC API rate limit issues encountered while creating templates with a large number of queue, skill, and agent assignment options.</h6></li>
                                            <li><h6>To guarantee optimal performance, we have limited the number of queues, skills, and agents that can be assigned to a Template. The restrictions are,</h6></li>
                                            <ul style={{ margin: '15px 35px' }}>
                                                <li>500 maximum queues</li>
                                                <li style={{ marginTop: '5px' }}>150 maximum skills</li>
                                                <li style={{ marginTop: '5px' }}>500 maximum agents</li>
                                            </ul>

                                            <li><h6>For Scheduled Templates, restrict template assignments to</h6></li>
                                            <ul style={{ margin: '15px 35px' }}>
                                                <li>100 maximum queues</li>
                                                <li style={{ marginTop: '5px' }}>100 maximum skills</li>
                                                <li style={{ marginTop: '5px' }}>100 maximum agents</li>
                                            </ul>
                                            <br />
                                            <li><h6>When the template is saved, any modifications to the queue or skill settings, for example, are applied to the current list of agents. When using a schedule template, the template configuration is stored, but the agent configurations are modified only when the scheduler runs.</h6></li>
                                        </ul>
                                    </Card.Text>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="0">
                                November 10, 2022 - Version 2.2
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Card.Text>
                                        <ul>
                                            <li><h6>Now, User Template assignment can be scheduled for a later time and date . Scheduling options are,</h6></li>
                                            <ul style={{ margin: '15px 35px' }}>
                                                <li>One time scheduler</li>
                                                <li>Recursive – Daily, Weekly & Monthly</li>
                                                <li>Option to repeat after x days</li>
                                            </ul>
                                            <li><h6>On the "Scheduled Template" tab, view the list of scheduled templates or edit/delete the scheduler.</h6></li>
                                        </ul>
                                    </Card.Text>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Container>
            </div>
        )
    }
}

export default ReleaseUpdate;