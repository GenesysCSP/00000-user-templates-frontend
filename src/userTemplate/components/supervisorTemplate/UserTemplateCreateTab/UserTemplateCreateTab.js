import React, { Component } from 'react';
import FilteredMultiSelect from 'react-filtered-multiselect';
import { Fade, Input, Label, Button } from 'reactstrap';
import { Row, Col, Alert } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import 'bootstrap/dist/css/bootstrap.min.css'
import Select from 'react-select';
import Ionicon from 'react-ionicons';
import classNames from 'classnames';
import './UserTemplateCreateTab.scss';
import 'react-select/dist/react-select.css';
import SkillList from '../SkillList/SkillList';
import utils from '../../../utilities/utils';
import { connect } from 'react-redux';
import { setTemplates } from '../../../actions/userActions'
import AlertModal from '../../Modals/AlertModal/AlertModal'
import Switch from '@material-ui/core/Switch';

export class UserTemplateCreateTab extends Component {
  state = {
    showHelp: false,
    saveError: '',
    selectedSkills: [],
    selectedQueues: [],
    availableSkills: [],
    newTemplateName: '',
    isNotValidText: false,
    isScheduleType: false
  };
  colorGenesysOrange = '#f15833';
  helpMessage = 'Select which queues and skills you would like to be added to the new template. Dont refresh the browser or close the page while update is in progress.';
  duplicateError = 'There is already a template with that name.';
  errorMessageHugeSkill = "A template can only have a maximum of 150 assigned skills, thus please remove some allocated skills before adding additional skills to the template."
  errorMessageHugeQueue = "Please adjust your selection to include 500 queues total since a template can only have a maximum of 500 queues."

  static getDerivedStateFromProps(nextProps, prevState) {
    let availableSkills;
    const allSkills = nextProps.allSkills;
    if (
      Array.isArray(prevState.availableSkills) &&
      prevState.availableSkills.length === 0 &&
      Array.isArray(allSkills) &&
      allSkills.length > 0
    ) {
      availableSkills = allSkills.map((object) => {
        return {
          ...object
        };
      });
    }
    if (availableSkills) {
      return { availableSkills };
    } else {
      return null;
    }
  }

  handleAddQueueSelection = (selectedQueues) => {
    if (selectedQueues.length > 500) {
      this.setState(() => ({ saveError: this.errorMessageHugeQueue }));
    } else {
      const sortedQueues = selectedQueues.sort(utils.compareValues('name'));
      this.setState(() => ({ selectedQueues: sortedQueues }));
    }
  };

  handleRemoveQueueSelection = (removedQueues) => {
    const selectedQueues = this.state.selectedQueues.slice();
    removedQueues.forEach((ro) => {
      const matchedQueueIndex = selectedQueues.findIndex((so) => so.id === ro.id);
      if (matchedQueueIndex > -1) {
        selectedQueues.splice(matchedQueueIndex, 1);
      }
    });
    const sortedQueues = selectedQueues.sort(utils.compareValues('name'));
    this.setState(() => ({ selectedQueues: sortedQueues }));
  };

  handleRemoveSkillSelection = (removedSkill) => {
    const selectedSkills = this.state.selectedSkills.filter(s => s !== removedSkill)
    this.setState({ selectedSkills });
    const newAvailableList = this.state.availableSkills;
    removedSkill.proficiency = "0";
    newAvailableList.push(removedSkill);
    const sortedSkills = newAvailableList.sort(utils.compareValues('name'));
    this.setState(() => ({ availableSkills: sortedSkills }));
  };

  handleTemplateChange = (event) => {
    const value = event.target.value;
    if (value.length > 20 || !(/^([.\-_A-Za-z0-9]+)$/.test(value))) {
      this.setState({ isNotValidText: true });
    } else {
      this.setState({ isNotValidText: false });
    }

    this.setState(() => ({ newTemplateName: value }));
  };

  handleSaveButtonClick = () => {
    let skillsToAdd = [];
    if (this.state.selectedSkills.length > 0) {
      skillsToAdd = this.state.selectedSkills.map((object) => {
        return {
          name: object.name,
          proficiency: object.proficiency,
          id: object.id,
        };
      });
    }
    let queuesToAdd = [];
    if (this.state.selectedQueues.length > 0) {
      queuesToAdd = this.state.selectedQueues.map((object) => {
        return {
          name: object.name,
          id: object.id,
        }
      });
    }
    let newTemplateName = this.state.newTemplateName;
    if (this.props.templates.filter(function (e) { return e.displayName === newTemplateName }).length > 0) {
      this.setState(() => ({ saveError: this.duplicateError }));
    }
    else {
      const templateList = this.props.templates;

      templateList.push({
        displayName: this.state.newTemplateName,
        queueList: queuesToAdd,
        skillList: skillsToAdd
      });

      this.props.setTemplates(templateList)
      this.props.saveConfig()
        .then(() => {
          this.setState(() => ({
            newTemplateName: "",
            selectedQueues: [],
            selectedSkills: [],
            availableSkills: this.props.allSkills
          }))
        })
    }
  }

  handleSkillSelectionChange = (selectedOption) => {
    if (this.state.selectedSkills.length > 150) {
      this.setState(() => ({ saveError: this.errorMessageHugeSkill }));
    } else {
      if (selectedOption != null) {
        const newSkillList = this.state.selectedSkills;
        newSkillList.push(selectedOption);
        const sortedSkills = newSkillList.sort(utils.compareValues('name'));
        this.setState(() => ({ selectedSkills: sortedSkills }));
        const availableSkills = this.state.availableSkills.filter(s => s !== selectedOption)
        this.setState({ availableSkills });
      }
    }
  }

  handleProficiencyChange = (proficiency, skill) => {
    const selectedSkills = this.state.selectedSkills;
    selectedSkills[selectedSkills.indexOf(skill)].proficiency = proficiency;
    this.setState({ selectedSkills })
  }

  toggleHelp = () => {
    this.setState(() => ({ showHelp: !this.state.showHelp }));
  };

  clearSaveError = () => {
    this.setState(() => ({ saveError: '' }));
  };

  handleSchedulerChange = () => {
    this.setState((prevState) => ({ isScheduleType: !prevState.isScheduleType }));
  }


  render() {
    return (
      <div className="user-template-create-tab">
        <Container>
          <AlertModal
            isOpen={!!this.state.saveError}
            toggle={this.clearSaveError}
            header="Error Saving Configuration"
            body={this.state.saveError}
            onButtonClick={this.clearSaveError}
          />
          {/* <Alert variant="warning" isOpen={this.props.updatesPending} toggle={this.props.dismissUpdate}>
            Updated configurations available2.
          <Button
              className="float-right"
              color="warning"
              onClick={this.props.updateConfig}
              size="sm"
            >
              Refresh.
          </Button>
          </Alert> */}
          <div
            className={classNames({
              'user-template-sup-tab__help': true,
              'user-template-sup-tab__help--open': this.state.showHelp,
            })}
          >
            {this.state.showHelp && (
              <Fade in={this.state.showHelp}>
                <div>
                  <Alert variant="success">
                    {this.helpMessage}
                  </Alert>
                </div>
              </Fade>
            )}
            <Ionicon
              className="user-template-sup-tab__help-icon"
              color={this.colorGenesysOrange}
              fontSize="1.5rem"
              icon="md-help-circle"
              onClick={this.toggleHelp}
            />
          </div>
          <div className="user-template-create-tab__input">
            <Label for="newTemplateNameField">
              New Template Name
            </Label>
            <Input
              type="text"
              id="newTemplateNameField"
              value={this.state.newTemplateName}
              onChange={this.handleTemplateChange}
            />
            {(this.state.isNotValidText) &&
              <span className='error-text'>Template Name should be composed of alphanumeric characters and the special characters (-._), with a length of 20.</span>
            }
          </div>
          <br />
          {/* <div className="user-template-create-tab__input">
            <Label> && this.state.isScheduleType
              Template Type :
            </Label>
            <Switch
              checked={this.state.isScheduleType}
              onChange={this.handleSchedulerChange}
              inputProps={{ 'aria-label': 'controlled' }}
              color="primary"
            /> 
             {(this.state.isScheduleType) &&
              <span>Schedule</span>
            }
            this.state.isScheduleType && 
          </div> */}
          <br />
          <div className="user-template-create-tab__content">
            <Row className='user-template-create-tab-row'>
              <Col>
                <div className="user-template-create-tab__select-container">
                  <Label>
                    Available Queues
                  </Label>
                  <FilteredMultiSelect
                    className="user-template-create-tab__filtered-multi-select"
                    classNames={{
                      button: 'btn btn btn-block btn-default',
                      buttonActive: 'btn btn btn-block btn-primary',
                      filter: 'form-control',
                      select: 'form-control user-template-create-tab__select',
                    }}
                    buttonText="Add"
                    placeholder="Type to filter..."
                    options={this.props.allQueues}
                    onChange={this.handleAddQueueSelection}
                    selectedOptions={this.state.selectedQueues}
                    valueProp="id"
                  />
                </div>
              </Col>
              <Col>
                <div className="user-template-create-tab__select-container">
                  <div className="user-template-create-tab__select-title">
                    {`Added Queues (Max 500)`}
                  </div>
                  <FilteredMultiSelect
                    className="user-template-create-tab__filtered-multi-select"
                    classNames={{
                      button: 'btn btn btn-block btn-default',
                      buttonActive: 'btn btn btn-block btn-danger',
                      filter: 'form-control',
                      select: 'form-control user-template-create-tab__select',
                    }}
                    buttonText="Remove"
                    placeholder="Type to filter..."
                    options={this.state.selectedQueues}
                    onChange={this.handleRemoveQueueSelection}
                    valueProp="id"
                  />
                </div>
              </Col>
            </Row>
          </div>

          <br />
          <div className="user-template-create-tab__input">
            <Label>
              Select a Skill (Max 150)
            </Label>
            <Select
              placeholder="Type to filter..."
              onChange={this.handleSkillSelectionChange}
              options={this.state.availableSkills}
            />
          </div>
          <div className="user-template-create-tab__content">
            <div className="user-template-create-tab__scroll">
              <div className="user-template-create-tab__select">
                <SkillList skills={this.state.selectedSkills}
                  removeSkill={this.handleRemoveSkillSelection}
                  proficiencyChange={this.handleProficiencyChange} />
              </div>
            </div>
          </div>
          <br />
          <div className="user-template-create-tab__save-button">
            <Button
              color="success"
              onClick={this.handleSaveButtonClick}
              disabled={!this.state.newTemplateName || this.state.isNotValidText}>
              Save New Template
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  templates: state.templates,
  allSkills: state.allSkills,
  allQueues: state.allQueues,
  updatesPending: state.updatesPending
});

const mapDispatchToProps = (dispatch) => ({
  setTemplates: (templates) => dispatch(setTemplates(templates)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTemplateCreateTab);
