import React, { Component } from 'react';
import FilteredMultiSelect from 'react-filtered-multiselect';
import { Fade, Label, Button } from 'reactstrap';
import { Row, Col, Alert } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Select from 'react-select';
import Ionicon from 'react-ionicons';
import classNames from 'classnames';
import './UserTemplateEditTab.scss';
import SkillList from '../SkillList/SkillList';
import utils from '../../../utilities/utils'
import { connect } from 'react-redux';
import { setTemplates, deleteTemplate } from '../../../actions/userActions'
import { deleteScheduledTemplates } from '../../../services/aws';
import cloneDeep from 'lodash.clonedeep'
import { compareValues } from '../../../utilities/utils';
import { config } from '../../../../config/config'
import { addRemoveQueues, addSkills, removeSkills, removeUserSkills, setGCUserTemplateMapping, setGCUserTemplateMappings } from '../../../services/purecloud'
import WaitingModal from '../../Modals/WaitingModal/WaitingModal';
import { setUserTemplateMap } from '../../../actions/userActions'
import ConfirmDailog from '../dailogs/confirmAlert';
import utilities from '../../../utilities/utils';
import moment from 'moment';
import AlertModal from '../../Modals/AlertModal/AlertModal'

export class UserTemplateEditTab extends Component {
  state = {
    showHelp: false,
    showConfig: false,
    selectedSkills: [],
    selectedQueues: [],
    selectedTemplate: null,
    selectedSkill: '',
    availableSkills: [],
    savingConfig: false,
    modalHeader: "Updating User Skills and Queues",
    openAlert: false,
    isDelete: false,
  };
  oldSelectedQueue = [];
  oldSelectedSkill = [];
  confirmationMessage = "Would you like to edit the queue and skills? The changes will apply to all assigned agents."
  colorGenesysOrange = '#f15833';
  helpMessage = 'Select the template to update the queues and skills mapped to the template. Do not refresh the browser or close the page while update is in progress.';
  errorMessageHugeSkill = "A template can only have a maximum of 150 assigned skills, thus please remove some allocated skills before adding additional skills to the template."
  errorMessageHugeQueue = "A template can only have a maximum of 500 assigned queues, thus please remove some allocated queues before adding additional queues to the template. "

  static getDerivedStateFromProps(nextProps, prevState) {
    let availableSkills;
    const allSkills = nextProps.allSkills;
    if (
      Array.isArray(prevState.availableSkills) &&
      prevState.availableSkills.length === 0 &&
      Array.isArray(allSkills) &&
      allSkills.length > 0
    ) {
      availableSkills = allSkills.map((object) => {
        return {
          ...object
        };
      });
    }
    if (availableSkills) {
      return { availableSkills };
    } else {
      return null;
    }
  }

  handleAddQueueSelection = (selectedQueues) => {
    if (selectedQueues.length > 500) {
      this.setState(() => ({ saveError: this.errorMessageHugeQueue }));
    } else {
      const sortedQueues = selectedQueues.sort(utils.compareValues('name'));
      this.setState(() => ({ selectedQueues: sortedQueues }));
    }
  };

  handleRemoveQueueSelection = (removedQueues) => {
    const selectedQueues = this.state.selectedQueues.slice();
    removedQueues.forEach((ro) => {
      const matchedQueueIndex = selectedQueues.findIndex((so) => so.id === ro.id);
      if (matchedQueueIndex > -1) {
        selectedQueues.splice(matchedQueueIndex, 1);
      }
    });
    const sortedQueues = selectedQueues.sort(utils.compareValues('name'));
    this.setState(() => ({ selectedQueues: sortedQueues }));
  };

  handleRemoveSkillSelection = (removedSkill) => {
    const selectedSkills = this.state.selectedSkills.filter(s => s !== removedSkill)
    this.setState({ selectedSkills });
    const newAvailableList = this.state.availableSkills;
    removedSkill.proficiency = "0";
    newAvailableList.push(removedSkill);
    const sortedSkills = newAvailableList.sort(utils.compareValues('name'));
    this.setState(() => ({ availableSkills: sortedSkills }));
  };


  handleSkillSelectionChange = (selectedSkill) => {
    if (this.state.selectedSkills.length > 150) {
      this.setState(() => ({ saveError: this.errorMessageHugeSkill }));
    } else {
      if (selectedSkill != null) {
        const newSkillList = this.state.selectedSkills;
        newSkillList.push(selectedSkill);
        const sortedSkills = newSkillList.sort(utils.compareValues('name'));
        this.setState(() => ({ selectedSkills: sortedSkills }));
        const availableSkills = this.state.availableSkills.filter(s => s !== selectedSkill)
        this.setState({ availableSkills });
      }
    }
  }

  handleTemplateSelectionChange = (selectedTemplate) => {
    this.setState(() => ({ selectedTemplate }));
    if (selectedTemplate == null) {
      this.handleUnsetConfig();
    }
    else {
      this.handleSetConfig(selectedTemplate);
    }
  }

  handleUnsetConfig = () => {
    this.setState(() => ({
      showConfig: false,
      selectedQueues: [],
      selectedSkills: [],
      selectedTemplate: null,
      availableSkills: this.props.allSkills
    }));
  }

  handleSetConfig = (selectedTemplate) => {
    let selectedQueues = [];
    for (var queueId in selectedTemplate.queueList) {
      selectedQueues.push(
        this.props.allQueues.find(queue => { return queue.id === selectedTemplate.queueList[queueId].id })
      );
    }
    const sortedQueues = selectedQueues.filter(x => x != undefined).sort(utils.compareValues('name'));

    let selectedSkills = [];
    let availableSkills = cloneDeep(this.props.allSkills);
    for (var skillId in selectedTemplate?.skillList) {
      let newSkill = availableSkills.find(skill => { return skill.id === selectedTemplate.skillList[skillId].id });
      if (newSkill != undefined) {
        newSkill.proficiency = selectedTemplate.skillList[skillId].proficiency;
        selectedSkills.push(newSkill);
        availableSkills = availableSkills.filter(s => s.id !== newSkill.id);
      }
    }

    const sortedSkills = selectedSkills.sort(utils.compareValues('name'));
    this.oldSelectedSkill = [...sortedSkills];
    this.oldSelectedQueue = [...sortedQueues];

    this.setState(() => ({
      selectedQueues: sortedQueues,
      selectedSkills: sortedSkills,
      showConfig: true,
      selectedTemplate: selectedTemplate.label,
      availableSkills
    }));
  }

  handleProficiencyChange = (proficiency, skill) => {
    const selectedSkills = this.state.selectedSkills;
    selectedSkills[selectedSkills.indexOf(skill)].proficiency = proficiency;
    this.setState({ selectedSkills })
  }

  updateQueueSkills = async () => {
    const tempUserMap = this.props.userTemplateMap.find(x => x.templateName == this.state.selectedTemplate);
    let currentUsers = tempUserMap.userId ? tempUserMap.userId : [];

    // New added Skills
    let newAddedQueue = utils.differenceOf2Arrays(this.state.selectedQueues, this.oldSelectedQueue);
    let newAddedSkill = utils.differenceOf2Arrays(this.state.selectedSkills, this.oldSelectedSkill);

    //Removed queues and skills
    let removedQueues = utils.differenceOf2Arrays(this.oldSelectedQueue, this.state.selectedQueues);
    let removedSkill = utils.differenceOf2Arrays(this.oldSelectedSkill, this.state.selectedSkills);

    if (removedSkill?.length > 0) {
      await this.handleLoop(removedQueues, currentUsers, 'removeSkill');
      await this.handleLoop(this.state.selectedSkills, currentUsers, 'addSkill');
    } else {
      if (newAddedSkill?.length > 0) {
        await this.handleLoop(newAddedSkill, currentUsers, 'addSkill');
      }
    }

    if (removedQueues?.length > 0) {
      await this.handleLoop(removedQueues.map(x => x.id), currentUsers, 'removeQueue');
    }
    if (newAddedQueue?.length > 0) {
      await this.handleLoop(newAddedQueue.map(x => x.id), currentUsers, 'addQueue');
    }

  }

  handleSaveButtonClick = async () => {
    this.setState(() => ({ openAlert: true }));
  }


  handleDeleteButtonClick = async () => {
    this.confirmationMessage = 'Are you sure you want to delete the template? If you proceed, the assigned agent will lose their Skills and Queues.'
    this.setState(() => ({ isDelete: true, openAlert: true }));
  }

  toggleHelp = () => {
    this.setState(() => ({ showHelp: !this.state.showHelp }));
  };

  updateTemplate = async () => {
    if (this.state.isDelete) {
      await this.deleteTemplate();
    } else {
      if (this.state.selectedSkills.length > 150 && this.state.selectedQueues.length > 500) {
        this.setState(() => ({ saveError: this.state.selectedSkills.length > 150 ? this.errorMessageHugeSkill : this.state.selectedQueues.length > 500 ? this.errorMessageHugeSkill : "", openAlert: false }));
        return
      }
    }
    this.setState(() => ({ modalHeader: "Updating User Skills and Queues", openAlert: false, savingConfig: true }));
    const tempUserMap = this.props.userTemplateMap.find(x => x.templateName == this.state.selectedTemplate);

    if (tempUserMap?.userId.length > 0 && !this.state.isDelete && Object.keys(tempUserMap?.scheduleDetails).length === 0 || tempUserMap?.scheduleDetails?.startDate < moment().toISOString() && !tempUserMap?.scheduleDetails?.isRecursive) {
      await this.updateQueueSkills();
    }

    let skillsToAdd = [];
    if (this.state.selectedSkills.length > 0) {
      skillsToAdd = this.state.selectedSkills.map((object) => {
        return {
          name: object.name,
          proficiency: object.proficiency,
          id: object.id,
        };
      });
    }
    let queuesToAdd = [];
    if (this.state.selectedQueues.length > 0) {
      queuesToAdd = this.state.selectedQueues.map((object) => {
        return {
          name: object.name,
          id: object.id,
        }
      });
    }

    const templateList = this.props.templates;
    let template = templateList.find(e => { return e.displayName === this.state.selectedTemplate });
    if (template) {
      template.skillList = skillsToAdd;
      template.queueList = queuesToAdd;
      this.props.setTemplates(templateList);
    }
    this.setState(() => ({ savingConfig: false }));
    this.props.saveConfig();
  }

  async deleteTemplate() {
    this.setState(() => ({ modalHeader: "Deleting User Skills and Queues", openAlert: false, savingConfig: true }));
    const mappedUser = this.props.userTemplateMap.find(x => x.templateName == this.state.selectedTemplate);
    if (mappedUser && mappedUser.userId.length > 0) {
      const userId = mappedUser.userId;
      if (this.state.selectedSkills && this.state.selectedSkills.length > 0) {
        await this.handleLoop([userId.map(x => x)], userId, 'removeSkill');
      }
      if (this.state.selectedQueues && this.state.selectedQueues.length > 0) {
        let queueList = this.state.selectedQueues ? this.state.selectedQueues.map(x => x.id) : [];
        await this.handleLoop(queueList, mappedUser.userId, 'removeQueue');
      }
    }

    const newtemplateList = this.props.templates.filter(s => s.displayName !== this.state.selectedTemplate);
    const templateList = this.props.templates;
    let template = templateList.find(e => { return e.displayName === this.state.selectedTemplate })
    await this.props.setTemplates(newtemplateList);

    this.props.saveConfig().then(() => {
      this.handleUnsetConfig()
    }).then(() => {
      const userTempDelet = this.props.userTemplateMap.filter(s => s.templateName !== template.displayName)
      this.props.setUserTemplateMap(userTempDelet);
      deleteScheduledTemplates(
        `${config.backendEndpoint}template/schedules?templateName=${template.displayName}&type=permDelete`,
        utils.accessToken(),
        utils.getEnv()
      )
    })
    this.setState(() => ({ isDelete: false }))
  }

  async handleLoop(skillQueue, user, type) {
    for (let j = 0; j < skillQueue.length; j++) {
      switch (type) {
        case 'removeQueue':
          await addRemoveQueues(utils.chunksArray(user, 100), true, skillQueue);
          break;
        case 'removeSkill':
          await removeSkills(user, this.props.allSkills[0]);
          break;
        case 'addQueue':
          await addRemoveQueues(utils.chunksArray(user, 100), false, skillQueue);
          break;
        case 'addSkill':
          await addSkills(user, utils.handleSkillsList(skillQueue), 'PATCH');
          break;
        default:
          break;
      }
    }
  }

  clearSaveError = () => {
    this.setState(() => ({ saveError: '' }));
  }

  render() {
    return (
      <div className="user-template-edit-tab">
        <Container>
          <AlertModal
            isOpen={!!this.state.saveError}
            toggle={this.clearSaveError}
            header="Error Saving Configuration"
            body={this.state.saveError}
            onButtonClick={this.clearSaveError}
          />
          <WaitingModal isOpen={this.state.savingConfig} header={this.state.modalHeader} />
          <ConfirmDailog open={this.state.openAlert} title={"Confirmation"} confirm={this.updateTemplate} subject={this.confirmationMessage} handleClose={() => { this.setState({ openAlert: false }); }}></ConfirmDailog>
          <div
            className={classNames({
              'user-template-sup-tab__help': true,
              'user-template-sup-tab__help--open': this.state.showHelp,
            })}
          >
            {this.state.showHelp && (
              <Fade in={this.state.showHelp}>
                <div>
                  <Alert variant="success">{this.helpMessage}
                  </Alert>
                </div>
              </Fade>
            )}
            <Ionicon
              className="user-template-sup-tab__help-icon"
              color={this.colorGenesysOrange}
              fontSize="1.5rem"
              icon="md-help-circle"
              onClick={this.toggleHelp}
            />
          </div>
          <div className="user-template-edit-tab__input">
            <Label>
              Select a Template
            </Label>
            <Select
              placeholder="Type to filter..."
              value={this.state.selectedTemplate}
              onChange={this.handleTemplateSelectionChange}
              options={this.props.templates.sort(compareValues('displayName'))}
            />
          </div>
          <br />
          <div className="user-template-edit-tab__content">
            <Row className='user-template-edit-tab-row'>
              <Col>
                <div className="user-template-edit-tab__select-container">
                  <div className="user-template-edit-tab__select-title">
                    {'Available Queues'}
                  </div>
                  <FilteredMultiSelect
                    className="user-template-edit-tab__filtered-multi-select"
                    classNames={{
                      button: 'btn btn btn-block btn-default',
                      buttonActive: 'btn btn btn-block btn-primary',
                      filter: 'form-control',
                      select: 'form-control user-template-edit-tab__select',
                    }}
                    buttonText="Add"
                    placeholder="Type to filter..."
                    options={this.props.allQueues}
                    onChange={this.handleAddQueueSelection}
                    selectedOptions={this.state.selectedQueues}
                    valueProp="id"
                  />
                </div>
              </Col>
              <Col>
                <div className="user-template-edit-tab__select-container">
                  <div className="user-template-edit-tab__select-title">
                    {'Added Queues (Max 500)'}
                  </div>
                  <FilteredMultiSelect
                    className="user-template-edit-tab__filtered-multi-select"
                    classNames={{
                      button: 'btn btn btn-block btn-default',
                      buttonActive: 'btn btn btn-block btn-danger',
                      filter: 'form-control',
                      select: 'form-control user-template-edit-tab__select',
                    }}
                    buttonText="Remove"
                    placeholder="Type to filter..."
                    options={this.state.selectedQueues}
                    onChange={this.handleRemoveQueueSelection}
                    valueProp="id"
                  />
                </div>
              </Col>
            </Row>
          </div>
          <br />
          <div className="user-template-edit-tab__input">
            <Label>
              Select a Skill (Max 150)
            </Label>
            <Select
              placeholder="Type to filter..."
              onChange={this.handleSkillSelectionChange}
              options={this.state.availableSkills}
            />
          </div>
          <div className="user-template-edit-tab__content">
            <div className="user-template-edit-tab__scroll">
              <div className="user-template-edit-tab__select">
                <SkillList skills={this.state.selectedSkills}
                  removeSkill={this.handleRemoveSkillSelection}
                  proficiencyChange={this.handleProficiencyChange} />
              </div>
            </div>
          </div>
          <br />
          <div className="user-template-edit-tab__save-button">
            <Button
              color="success"
              onClick={this.handleSaveButtonClick}
              disabled={!this.state.selectedTemplate} >
              Save Changes
            </Button>
            <Button
              color="danger" className="user-template-edit-tab__del-button"
              onClick={this.handleDeleteButtonClick}
              disabled={!this.state.selectedTemplate}>
              Delete Template
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  templates: state.templates,
  userTemplateMap: state.userTemplateMap,
  allSkills: state.allSkills,
  allQueues: state.allQueues,
  updatesPending: state.updatesPending
});

const mapDispatchToProps = (dispatch) => ({
  setTemplates: (templates) => dispatch(setTemplates(templates)),
  deleteTemplate: (templateToRemove) => dispatch(deleteTemplate(templateToRemove)),
  setUserTemplateMap: (userTemplateMap) => dispatch(setUserTemplateMap(userTemplateMap)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTemplateEditTab);
