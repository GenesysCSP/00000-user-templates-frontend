import * as React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { WarningRounded } from '@material-ui/icons';
import "../scheduleTemplateTab/schedule.css"

const ConfirmDailog = (props) => {
    return (
        <Dialog
            open={props.open}
            keepMounted
            aria-describedby="alert-dialog-slide-description"
            className='alert-dailog'
        >
            <DialogTitle className='confirm-title'>
                <WarningRounded />  {props.title}
            </DialogTitle>
            <DialogContent>
                <div className='deleteMessage'>
                    {props.subject}
                </div>
            </DialogContent>
            <DialogActions >
                <Button onClick={props.confirm}>Ok</Button>
                <Button onClick={props.handleClose}>Cancel</Button>
            </DialogActions>
        </Dialog>
    );
}

export default ConfirmDailog;