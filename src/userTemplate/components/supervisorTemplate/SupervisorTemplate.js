import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import 'react-web-tabs/dist/react-web-tabs.css';
import './SupervisorTemplate.scss';

import { checkForAuthErrors } from '../../actions/authActions';
import aws from '../../services/aws';
import { fetchSkills, fetchQueues } from '../../services/purecloud';
import WaitingModal from '../Modals/WaitingModal/WaitingModal';
import MessageModal from '../Modals/MessageModal/MessageModal';
import AlertModal from '../Modals/AlertModal/AlertModal';
import UserTemplateCreateTab from './UserTemplateCreateTab/UserTemplateCreateTab';
import UserTemplateEditTab from './UserTemplateEditTab/UserTemplateEditTab';
import UserTemplateApplyTab from './UserTemplateApplyTab/UserTemplateApplyTab'
import utils from '../../utilities/utils'
import { setTemplates, setUserTemplateMap, setSkills, setQueues, setUpdatesPending } from '../../actions/userActions'
import { config } from '../../../config/config'
import SheduleTemplateTab from './scheduleTemplateTab/scheduledTemplateList';

export class SupervisorTemplate extends Component {

  loadingError = 'There was an error loading the configuration. Please refresh ' +
    'your browser to try again. If this message persists, please contact your ' +
    'your support personnel for assistance.';
  saveError = 'There was an error saving the configuration. Please try again. ' +
    'If this message persists, please contact your support personnel for assistance.';
  duplicateError = 'There is already a template with that name.';

  state = {
    loadingConfig: true,
    loadingError: '',
    saveError: '',
    savingConfig: false,
    showSaveConfirmation: false,
    skillsToAdd: [],
    queuesToAdd: [],
    timerId: '',
    isSchedulerTab: false
  };

  componentDidMount() {
    // var timerId = setInterval(this.pollForUpdates, 60000)
    // this.setState(() => ({ timerId, loadingConfig: true }));

    this.getConfigurations().then((result) => {
      try {
        if (result && result.success) {
          this.props.setTemplates(result.templates)
          this.props.setUserTemplateMap(result.templateMap)
          this.props.setQueues(result.queues)
          this.props.setSkills(result.skills)
          this.setState(() => ({ loadingConfig: false }));
        }
        else {
          this.setState(() => ({ loadingConfig: false }));
          const isAuthError = this.props.checkForAuthErrors(result.error);
          if (!isAuthError) {
            this.setState(() => ({ loadingError: this.loadingError }));
          } else {
            // Nothing to be done. Auth errors will automatically take
            // the user back to the login page to re-authenticate
          }
        }
      }
      catch (error) {
        this.setState(() => ({ loadingConfig: false, loadingError: this.loadingError }));
        console.log(error)
      }
    })
  }

  componentWillUnmount() {
    clearInterval(this.state.timerId)
  }

  pollForUpdates = () => {
    this.getConfigurations().then((result) => {
      if (result && result.success) {
        let templates = result.templates.map((template) => ({
          value: template.displayName,
          label: template.displayName,
          ...template
        }))
        let skills = result.skills.map((skill) => ({
          value: skill.name,
          label: skill.name,
          proficiency: "0",
          ...skill
        }))
        if (JSON.stringify(templates) !== JSON.stringify(this.props.templates)) {
          this.props.setUpdatesPending(true)
          clearInterval(this.state.timerId)
        }
        else if (JSON.stringify(result.queues) !== JSON.stringify(this.props.allQueues)) {
          this.props.setUpdatesPending(true)
          clearInterval(this.state.timerId)
        }
        else if (JSON.stringify(result.templateMap) !== JSON.stringify(this.props.userTemplateMap)) {
          this.props.setUpdatesPending(true)
          clearInterval(this.state.timerId)
        }
        else if (JSON.stringify(skills) !== JSON.stringify(this.props.allSkills)) {
          this.props.setUpdatesPending(true)
          clearInterval(this.state.timerId)
        }
        else {
        }
      }
      else {

      }
    })
  }

  getConfigurations = () => {
    return new Promise(resolve => {
      let returnValue = {
        queues: [],
        skills: [],
        templates: [],
        templateMap: [],
        success: true,
        message: ''
      }
      const promises = [
        fetchSkills(),
        fetchQueues(),
        aws.fetchApplicationConfig(
          `${config.backendEndpoint}templates`,
          utils.accessToken(),
          utils.getEnv()
        ),
        aws.fetchUserTemplateMapping(
          `${config.backendEndpoint}template/schedules`,
          utils.accessToken(),
          utils.getEnv()
        )
      ];
      Promise.all(promises)
        .then((result) => {
          const skillEntities = result[0].entities;
          const queueEntities = result[1].entities;
          const configResult = result[2];
          const mapConfigResult = result[3]

          const allSkills = skillEntities.map((entity) => ({
            ...entity,
            text: entity.name,
          }));

          const allQueues = queueEntities.map((entity) => ({
            ...entity,
            text: entity.name,
          }));

          const appConfig = configResult.config.Item;
          if (configResult.success && !utils.isObjectEmpty(appConfig)) {
            let templateList = appConfig.templateList
            var newTemplateList = []
            for (var template in templateList) {
              var newTemplate = {
                displayName: templateList[template].displayName,
                queueList: [],
                skillList: []
              }
              for (var skill in templateList[template].skillList) {
                if (allSkills.find(skillToCheck => { return skillToCheck.id === templateList[template].skillList[skill].id })) {
                  newTemplate.skillList.push(templateList[template].skillList[skill])
                }
              }
              for (var queue in templateList[template].queueList) {
                if (allQueues.find(queueToCheck => { return queueToCheck.id === templateList[template].queueList[queue].id })) {
                  newTemplate.queueList.push(templateList[template].queueList[queue])
                }
              }
              newTemplateList.push(newTemplate)
            }
            returnValue.templates = newTemplateList
          }

          if (mapConfigResult.success) {
            returnValue.templateMap = mapConfigResult.message
          };

          returnValue.queues = allQueues
          returnValue.skills = allSkills
          resolve(returnValue)
        })
        .catch((error) => {
          returnValue.success = false
          returnValue.message = error
          console.log(error)
          resolve(returnValue)
        });
    })
  }

  saveNewTemplate = (newTemplateName, queuesToAdd, skillsToAdd) => {
    if (this.state.templateList.filter(function (e) { return e.displayName === newTemplateName; }).length > 0) {
      this.setState(() => ({ saveError: this.duplicateError }));
    }
    else {
      this.state.templateList.push({
        skillList: skillsToAdd,
        queueList: queuesToAdd,
        displayName: newTemplateName
      });
      this.saveTemplateConfiguration();
    }
  };

  saveTemplateConfiguration = () => {
    this.setState(() => ({ savingConfig: true }));
    const configUpdate = {
      templateList: this.props.templates.map((template) => ({
        displayName: template.displayName,
        queueList: template.queueList,
        skillList: template.skillList,
      })),
    };

    return new Promise((resolve, reject) => {
      aws.saveApplicationConfig(
        `${config.backendEndpoint}templates`,
        utils.accessToken(),
        utils.getEnv(),
        configUpdate
      )
        .then((result) => {
          this.setState(() => ({ savingConfig: false }));
          if (result.success) {
            this.toggleSaveConfirmation();
            resolve();
          } else {
            this.setState(() => ({ saveError: this.saveError }));
            reject();
          }
        })
        .catch((error) => {
          this.setState(() => ({ savingConfig: false }));
          const isAuthError = this.props.checkForAuthErrors(error);
          if (!isAuthError) {
            this.setState(() => ({ saveError: this.saveError }));
          } else {
            // Nothing to be done. Auth errors will automatically take
            // the user back to the login page to re-authenticate
          }
          reject();
        });
    })
  };


  updateConfigurations = () => {
    this.props.setUpdatesPending(false)
    this.setState(() => ({ loadingConfig: true }));
    this.getConfigurations().then((result) => {
      if (result && result.success) {
        this.props.setTemplates(result.templates)
        this.props.setUserTemplateMap(result.templateMap)
        this.props.setQueues(result.queues)
        this.props.setSkills(result.skills)
        this.setState(() => ({ loadingConfig: false }));
      }
      else {
        this.setState(() => ({ loadingConfig: false }));
        const isAuthError = this.props.checkForAuthErrors(result.error);
        if (!isAuthError) {
          this.setState(() => ({ loadingError: this.loadingError }));
        } else {
          // Nothing to be done. Auth errors will automatically take
          // the user back to the login page to re-authenticate
        }
      }
    })
    // var timerId = setInterval(this.pollForUpdates, 60000)
    // this.setState(() => ({ timerId }));
  }

  dismissUpdate = () => {
    this.props.setUpdatesPending(false)
    // var timerId = setInterval(this.pollForUpdates, 60000)
    // this.setState(() => ({ timerId }));
  }

  toggleSaveConfirmation = () => {
    this.setState((prevState) => ({ showSaveConfirmation: !prevState.showSaveConfirmation }));
  };

  clearSaveError = () => {
    this.setState(() => ({ saveError: '' }));
  };

  render() {
    return (
      <div className="user-template-tab">
        <WaitingModal isOpen={this.state.loadingConfig} header="Loading Configuration" />
        <WaitingModal isOpen={this.state.savingConfig} header="Saving Configuration" />
        <MessageModal
          isOpen={this.state.showSaveConfirmation}
          toggle={this.toggleSaveConfirmation}
          header="Save Confirmation"
          body="Your configuration changes were saved successfully"
          onButtonClick={this.toggleSaveConfirmation}
        />
        <AlertModal
          isOpen={!!this.state.saveError}
          toggle={this.clearSaveError}
          header="Error Saving Configuration"
          body={this.state.saveError}
          onButtonClick={this.clearSaveError}
        />
        <Tabs className="user-template-tab__tabs">
          <div className="user-template-tab__tab-list-container">
            <TabList className="user-template-tab__tab-list">
              <Tab className="user-template-tab__tab" tabFor="createTemplate">
                Create Template
              </Tab>
              <Tab className="user-template-tab__tab" tabFor="editTemplate">
                Edit Template
              </Tab>
              <Tab className="user-template-tab__tab" tabFor="usersTemplate">
                Users Template
              </Tab>
              <Tab className="user-template-tab__tab" tabFor="scheduleTemplate">
                Scheduled Template
              </Tab>
            </TabList>
          </div>
          <TabPanel className="user-template-tab__tab-panel" tabId="createTemplate">
            <UserTemplateCreateTab
              saveConfig={this.saveTemplateConfiguration}
              updateConfig={this.updateConfigurations}
              dismissUpdate={this.dismissUpdate}
            />
          </TabPanel>
          <TabPanel className="user-template-tab__tab-panel" tabId="editTemplate">
            <UserTemplateEditTab
              saveConfig={this.saveTemplateConfiguration}
              updateConfig={this.updateConfigurations}
              dismissUpdate={this.dismissUpdate}
            />
          </TabPanel>
          <TabPanel className="user-template-tab__tab-panel" tabId="usersTemplate">
            <UserTemplateApplyTab></UserTemplateApplyTab>
          </TabPanel>
          <TabPanel className="user-template-tab__tab-panel" tabId="scheduleTemplate">
            <SheduleTemplateTab></SheduleTemplateTab>
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  templates: state.templates,
  userTemplateMap: state.userTemplateMap,
  allQueues: state.allQueues,
  allSkills: state.allSkills,
  updatesPending: state.updatesPending
});

const mapDispatchToProps = (dispatch) => ({
  checkForAuthErrors: (error) => dispatch(checkForAuthErrors(error)),
  setTemplates: (templates) => dispatch(setTemplates(templates)),
  setUserTemplateMap: (templateMap) => dispatch(setUserTemplateMap(templateMap)),
  setQueues: (queues) => dispatch(setQueues(queues)),
  setSkills: (skills) => dispatch(setSkills(skills)),
  setUpdatesPending: (pending) => dispatch(setUpdatesPending(pending))
});

export default connect(mapStateToProps, mapDispatchToProps)(SupervisorTemplate);