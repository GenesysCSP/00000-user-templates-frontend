import React, { Component } from 'react';
import { Button } from 'reactstrap';
import Select from 'react-select';

import './SkillList.scss';

const skillProficiency = [
  { label: "0", value: "0" },
  { label: "1", value: "1" },
  { label: "2", value: "2" },
  { label: "3", value: "3" },
  { label: "4", value: "4" },
  { label: "5", value: "5" }
]

export class SkillList extends Component {
  render() {
    var skills = this.props.skills.map((skill) => {
      return (
        <SkillListItem key={skill.id} skill={skill}
          removeSkill={this.props.removeSkill}
          handleProficiencyChange={this.props.proficiencyChange}
        />
      );
    });
    return (
      <ul className="list-group"> {skills} </ul>
    );
  }
}

class SkillListItem extends React.Component {
  state = {
    selectedProficiency: "0"
  }
  static getDerivedStateFromProps(nextProps, prevState) {
    let selectedProficiency;
    const proficiency = nextProps.skill.proficiency;
    selectedProficiency = proficiency
    return { selectedProficiency };
  }
  onClickClose = () => {
    this.props.removeSkill(this.props.skill);
  }
  handleSelectionChange = (selectedProficiency) => {
    if (selectedProficiency == null) {
      this.setState(() => ({
        selectedProficiency: "0"
      }));
      this.props.handleProficiencyChange("0", this.props.skill)
    }
    else {
      this.setState(() => ({
        selectedProficiency
      }));
      this.props.handleProficiencyChange(selectedProficiency.label, this.props.skill)
    }
  }
  render() {
    return (
      <li className="list-group-item ">
        <div className="skill-list-tab__content">
          <div className="skill-list-tab__content-label">
            {this.props.skill.name}
          </div>
          <div className="skill-list-tab__content-select">
            <Select
              value={this.state.selectedProficiency}
              options={skillProficiency}
              onChange={this.handleSelectionChange}
            />
          </div>
          <div className="skill-list-tab__save-button">
            <Button id="btnRemoveSkill"
              color="danger"
              onClick={this.onClickClose}>
              Remove Skill
          </Button>
          </div>
        </div>
      </li>
    );
  }
}

export default SkillList;