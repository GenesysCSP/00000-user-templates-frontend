import React, { Component } from 'react';
import FilteredMultiSelect from 'react-filtered-multiselect';
import { Fade, Label, Button } from 'reactstrap';
import { Alert } from 'react-bootstrap'
import Container from 'react-bootstrap/Container'
import Select from 'react-select';
import Ionicon from 'react-ionicons';
import classNames from 'classnames';
import './UserTemplateApplyTab.css';
import utils, { compareValues } from '../../../utilities/utils';
import { connect } from 'react-redux';
import { config } from '../../../../config/config'
import { addScheduledTemplates, fetchUserTemplateMapping } from '../../../services/aws';
import { addRemoveQueues, addSkills, getAllQueues, removeSkills, setGCUserTemplateMapping, setGCUserTemplateMappings } from '../../../services/purecloud';
import { setUserTemplateMap, setIsFiltered, setIsUpdating } from '../../../actions/userActions'
import MessageModal from '../../Modals/MessageModal/MessageModal';
import AlertModal from '../../Modals/AlertModal/AlertModal';
import WaitingModal from '../../Modals/WaitingModal/WaitingModal'
import SchedulerDialog from '../scheduleTemplateTab/schedulerDialog';
import Switch from '@material-ui/core/Switch';
import moment from 'moment';
import utilities from '../../../utilities/utils';

export class UserTemplateApplyTab extends Component {
  state = {
    saveError: '',
    savingConfig: false,
    showSaveConfirmation: false,
    showHelp: false,
    selectedTemplate: null,
    templates: [],
    selectedUsers: [],
    selectedQueues: '',
    selectedSkills: '',
    openSheduler: false,
    isScheduledJob: false,
    buttonName: 'Apply Changes',
    isScheduleJobSave: "Your configuration changes were saved successfully",
    showLoader: false,
  };
  currentUserInTemplate = [];
  errorMessageExisitTemplate = "A template can only have a maximum of 500 assigned agents, thus please remove some allocated agents before adding additional agents to the template."
  errorMessageNewTemplate = "Please adjust your selection to include 500 agents total since a template can only have a maximum of 500 agents."
  headerMessage = "Saving configuration, please do not refresh the page.";
  saveConfirmationHeader = "Save Confirmation"
  throttleCount = 0;
  completedActivities = 0;
  colorGenesysOrange = '#f15833';
  helpMessage = 'Select a template from the dropdown, then add or remove users to the template. Click "Apply Changes" to save the new template assignments. Dont refresh the browser or close the page while update is in progress.';
  saveError = 'There was an error saving the configuration. Please try again. ' +
    'If this message persists, please contact your support personnel for assistance.';

  handleAddUserSelection = (selectedUsers) => {
    if (selectedUsers?.length > 500) {
      this.setState(() => ({ saveError: this.currentUserInTemplate?.length >= 500 ? this.errorMessageNewTemplate : selectedUsers?.length > 500 ? this.errorMessageExisitTemplate : "" }));
    } else {
      const sortedUsers = selectedUsers.sort(compareValues('name'));
      this.setState(() => ({ selectedUsers: sortedUsers }));
    }
  };

  handleRemoveUserSelection = (removedUsers) => {
    const selectedUsers = this.state.selectedUsers.slice();
    removedUsers.forEach((ro) => {
      const matchedUserIndex = selectedUsers.findIndex((so) => so.id === ro.id);
      if (matchedUserIndex > -1) {
        selectedUsers.splice(matchedUserIndex, 1);
      }
    });
    const sortedUsers = selectedUsers.sort(compareValues('name'));
    this.setState(() => ({ selectedUsers: sortedUsers }));
  };

  handleTemplateSelectionChange = (selectedTemplate) => {
    this.setState(() => ({ selectedTemplate }));
    if (selectedTemplate == null) {
      this.handleUnsetUsers();
    }
    else {
      this.handleSetUsers(selectedTemplate);
    }
  }

  handleUnsetUsers = () => {
    this.setState(() => ({
      selectedUsers: [],
      selectedTemplate: null,
      selectedQueues: '',
      selectedSkills: '',
      scheduleDetails: {},
      showLoader: false
    }));
  }

  handleSetUsers = (selectedTemplate) => {
    let selectedUsers = [];
    let selectedQueues = '';
    let selectedSkills = '';
    let scheduleDetails = {};

    let templateUsers = this.props.userTemplateMap.find(s => s.templateName === selectedTemplate.displayName);
    this.currentUserInTemplate = templateUsers?.userId;
    if (templateUsers?.userId) {
      for (var templateUser of templateUsers.userId) { //Hari
        let userToAdd = this.props.allUsers.find(user => { return user.id === templateUser })
        if (userToAdd) {
          selectedUsers.push(userToAdd);
        }
      }
      if (templateUsers.scheduleDetails !== undefined && Object.keys(templateUsers.scheduleDetails).length > 0) {
        scheduleDetails = templateUsers.scheduleDetails;
        this.setState({ isScheduledJob: true, buttonName: "Save Schedule Template" })
      } else {
        this.setState({ isScheduledJob: false, buttonName: "Apply Changes" })
      }
    } else {
      this.setState({ isScheduledJob: false, buttonName: "Apply Changes" });
    }
    for (var queue in selectedTemplate.queueList) {
      selectedQueues = selectedQueues ? selectedQueues + ", " + selectedTemplate.queueList[queue].name : selectedTemplate.queueList[queue].name
    }
    for (var skill in selectedTemplate.skillList) {
      selectedSkills = selectedSkills ? selectedSkills + ", " + selectedTemplate.skillList[skill].name : selectedTemplate.skillList[skill].name
    }

    const sortedUsers = selectedUsers.sort(compareValues('name'));
    this.setState(() => ({
      selectedUsers: sortedUsers,
      selectedQueues,
      selectedSkills,
      scheduleDetails,
      showLoader: false
    }));
  }

  handleSaveButtonClick = async () => {
    const tempUserMap = this.props.userTemplateMap
    this.setState(() => ({ savingConfig: true }));
    this.props.setIsUpdating(true);
    let selectedTemp = tempUserMap.find(x => x.templateName === this.state.selectedTemplate.displayName);
    if (!selectedTemp) {
      selectedTemp = {
        lastAppliedDate: "",
        orgId: this.props.orgId,
        scheduleDetails: {},
        templateName: this.state.selectedTemplate.displayName,
        userId: []
      };
      tempUserMap.push(selectedTemp)
    }
    let currentUsers = selectedTemp.userId ? selectedTemp.userId : [];
    let futureUsers = this.state.selectedUsers.map(x => x?.id);
    let removedUsers = currentUsers.filter(x => (!futureUsers.includes(x) && x !== "0"));
    const newUser = futureUsers.filter(x => !currentUsers.includes(x))

    selectedTemp.userId = futureUsers;
    selectedTemp.lastAppliedDate = this.state.isScheduledJob ? "" : moment().utc().format();
    selectedTemp.scheduleDetails = this.state.isScheduledJob ? this.state.scheduleDetails : {};
    this.state.selectedTemplate.userId = futureUsers;

    // saves mapped users in dynamo db
    let response = await addScheduledTemplates(
      `${config.backendEndpoint}template/schedules`,
      utils.accessToken(),
      utils.getEnv(),
      selectedTemp
    );

    if (response && response?.success) {
      if (!this.state.isScheduledJob) {
        // If removed users are greater tha one. Then need to remove the skills
        const removeQueue = await getAllQueues(newUser.concat(removedUsers));
        console.log(`*** Total queue to be removed from the Agents : ${removeQueue.length} ***`);
        console.log(`*** Total agents to be removed from the Skills : ${removedUsers.length} ***`);
        console.log(`*** Total new users for this template = ${newUser.length} ***`);
        if (removedUsers?.length > 0) {
          await removeSkills(removedUsers, this.props.allSkills[0]);
        }
        // It adds new assigned queues and skills
        this.addQueueSkill(newUser, removedUsers, removeQueue);
      } else {
        tempUserMap.forEach(function (i, n) {
          if (i.templateName === selectedTemp.templateName) {
            tempUserMap[n] = selectedTemp
          }
        });
        this.props.setUserTemplateMap(tempUserMap);
        this.setState(() => ({ savingConfig: false, isScheduleJobSave: "Your configuration changes were saved successfully" }));
        this.saveConfirmationHeader = "Save Confirmation";
        this.toggleSaveConfirmation();
      }
    } else {
      this.setState(() => ({ saveError: this.saveError }));
      this.setState(() => ({ savingConfig: false }))
    }
  }

  addQueueSkill = (futureUsers, removedUsers, queues) => {
    return new Promise(async () => {
      // It removes agents old queue and add new queue
      console.log(`*** Total queue need to set for the Agents = ${this.state.selectedTemplate.queueList.length} ***`);
      const isAddOrRemove = [true, false];
      for (let i = 0; i < isAddOrRemove.length; i++) {
        let users = isAddOrRemove[i] ? futureUsers.concat(removedUsers) : futureUsers;
        let queue = isAddOrRemove[i] ? queues : this.state.selectedTemplate.queueList.map(x => x.id);
        await addRemoveQueues(utils.chunksArray(users, 100), isAddOrRemove[i], queue);
      }

      // It adds assigned skills
      console.log(`*** Total skills need to set for the Agents = ${this.state.selectedTemplate.skillList.length} ***`);
      let requestType = "PUT";
      let chunkedSkills = utils.handleSkillsList(this.state.selectedTemplate.skillList);
      if (chunkedSkills.length > 0) {
        for (let j = 0; j < chunkedSkills.length; j++) {
          await addSkills(futureUsers, [chunkedSkills[j]], requestType);
          requestType = "PATCH";
        }
      } else {
        await removeSkills(futureUsers, this.props.allSkills[0]);
      }

      // await utilities.sleep(5000)
      this.setState(() => ({ savingConfig: false, isScheduleJobSave: "Your configuration changes were saved successfully" }));
      this.saveConfirmationHeader = "Save Confirmation";
      this.toggleSaveConfirmation()
      await fetchUserTemplateMapping(
        `${config.backendEndpoint}template/schedules`,
        utils.accessToken(),
        utils.getEnv()
      ).then(x => { this.props.setUserTemplateMap(x?.message); }).catch(err => console.log(err));
    })
  }

  handleSchedulerData = (data) => {
    this.setState({ openSheduler: false, buttonName: "Save Schedule Template", scheduleDetails: data });
  }

  handleDialogClose = () => {
    if (this.state.scheduleDetails !== undefined && Object.entries(this.state.scheduleDetails).length > 0) {
      this.setState({ openSheduler: false, isScheduledJob: true });
    } else {
      this.setState({ openSheduler: false, isScheduledJob: false });
    }
  }

  handleSchedulerChange = () => {
    if (this.state.isScheduledJob) {
      this.setState({ isScheduledJob: false, openSheduler: false, scheduleDetails: {}, buttonName: "Apply Changes" })
    }
    else {
      this.setState({ isScheduledJob: true, openSheduler: true })
    }
  }

  toggleHelp = () => {
    this.setState(() => ({ showHelp: !this.state.showHelp }));
  };
  toggleSaveConfirmation = () => {
    this.setState((prevState) => ({ showSaveConfirmation: !prevState.showSaveConfirmation }));
  };

  clearSaveError = () => {
    this.setState(() => ({ saveError: '' }));
  }

  handleSchedulerListChange = () => {
    this.setState({ openSheduler: true })
  }

  render() {
    return (
      <div className="user-template-sup-tab" style={{ height: "735px", overflow: "auto" }}>
        <Container>
          <WaitingModal isOpen={this.state.savingConfig} header={this.headerMessage} />
          <MessageModal
            isOpen={this.state.showSaveConfirmation}
            toggle={this.toggleSaveConfirmation}
            header={this.saveConfirmationHeader}
            body={this.state.isScheduleJobSave}
            onButtonClick={this.toggleSaveConfirmation}
          />
          <AlertModal
            isOpen={!!this.state.saveError}
            toggle={this.clearSaveError}
            header="Error Saving Configuration"
            body={this.state.saveError}
            onButtonClick={this.clearSaveError}
          />
          <div
            className={classNames({
              'user-template-sup-tab__help': true,
              'user-template-sup-tab__help--open': this.state.showHelp,
            })}
          >
            {this.state.showHelp && (
              <Fade in={this.state.showHelp}>
                <div>
                  <Alert variant="success">{this.helpMessage}
                  </Alert>
                </div>
              </Fade>
            )}
            <Ionicon
              className="user-template-sup-tab__help-icon"
              color={this.colorGenesysOrange}
              fontSize="1.5rem"
              icon="md-help-circle"
              onClick={this.toggleHelp}
            />
          </div>
          <div className="user-template-sup-tab__input">
            <Label>
              Select a Template
            </Label>
            <Select
              placeholder="Type to filter..."
              value={this.state.selectedTemplate}
              onChange={this.handleTemplateSelectionChange}
              options={this.props.isFiltered ? this.props.filteredTemplates : this.props.templates}
            />
            <br />
          </div>
          <br />
          <div className="user-template-sup-tab__input user-template-sup-tab__Queue">
            <Label>
              Queues:
            </Label>
            {' ' + this.state.selectedQueues}
          </div>
          <br />
          <div className="user-template-sup-tab__input user-template-sup-tab__Skill">
            <Label>
              Skills:
            </Label>
            {' ' + this.state.selectedSkills}
          </div>
          <br />
          <div className="user-template-sup-tab__input user-template-sup-tab__Skill">
            <Label>
              Schedule :
            </Label>
            <Switch
              checked={this.state.isScheduledJob}
              onChange={this.handleSchedulerChange}
              inputProps={{ 'aria-label': 'controlled' }}
              color="primary"
              disabled={this.state.selectedTemplate ? false : true}
            />
            {(this.state.scheduleDetails !== undefined) && (Object.keys(this.state.scheduleDetails).length > 0) &&
              <span className='schedulerList' onClick={this.handleSchedulerListChange}>
                <a><Label>Start Date</Label> :  {moment(this.state.scheduleDetails.startDate).format("YYYY-MM-DD")}    </a>
                {this.state.scheduleDetails.endDate && <a><Label>End Date</Label> : {moment(this.state.scheduleDetails.endDate).format("YYYY-MM-DD")}   </a>}

                <a> <Label>Schedule Type</Label> : {this.state.scheduleDetails.cronWordExp}</a>
              </span>
            }
            <SchedulerDialog dailogOpen={this.state.openSheduler} handleClose={this.handleDialogClose} isRepeat={false} schedulerData={this.state.scheduleDetails} toSaveData={this.handleSchedulerData} templateName={this.state.selectedTemplate} isUpdate={false} />
          </div>
          <br />
          <div className="user-template-sup-tab__content">
            <div className="user-template-sup-tab__select-container">
              <div className="user-template-sup-tab__select-title">
                {'Available Agents'}
              </div>
              <FilteredMultiSelect
                className="user-template-sup-tab__filtered-multi-select"
                classNames={{
                  button: 'btn btn btn-block btn-default',
                  buttonActive: 'btn btn btn-block btn-primary',
                  filter: 'form-control',
                  select: 'form-control user-template-sup-tab__select',
                }}
                buttonText="Add"
                placeholder="Type to filter..."
                options={this.props.allUsers.sort(compareValues('name'))}
                onChange={this.handleAddUserSelection}
                selectedOptions={this.state.selectedUsers.sort(compareValues('name'))}
                valueProp="id"
              />
            </div>
            <div className="user-template-sup-tab__select-container">
              <div className="user-template-sup-tab__select-title">
                {'Assigned Agents (Max 500)'}
              </div>
              <FilteredMultiSelect
                className="user-template-sup-tab__filtered-multi-select"
                classNames={{
                  button: 'btn btn btn-block btn-default',
                  buttonActive: 'btn btn btn-block btn-danger',
                  filter: 'form-control',
                  select: 'form-control user-template-sup-tab__select',
                }}
                buttonText="Remove"
                placeholder="Type to filter..."
                options={this.state.selectedUsers.sort(compareValues('name'))}
                onChange={this.handleRemoveUserSelection}
                valueProp="id"
              />
            </div>

          </div>
          <br />
          <div className="user-template-sup-tab__save-button">
            <Button
              color="success"
              className="user-template-button-loader"
              onClick={this.handleSaveButtonClick}
              disabled={!this.state.selectedTemplate}>
              {this.state.buttonName}
            </Button>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  templates: state.templates,
  allUsers: state.allUsers,
  orgId: state.orgId,
  allSkills: state.allSkills,
  userTemplateMap: state.userTemplateMap,
  isFiltered: state.isFiltered,
  filteredTemplates: state.filteredTemplates,
});

const mapDispatchToProps = (dispatch) => ({
  setUserTemplateMap: (userTemplateMap) => dispatch(setUserTemplateMap(userTemplateMap)),
  setIsFiltered: (isFiltered) => dispatch(setIsFiltered(isFiltered)),
  setIsUpdating: (isUpdating) => dispatch(setIsUpdating(isUpdating))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserTemplateApplyTab)
