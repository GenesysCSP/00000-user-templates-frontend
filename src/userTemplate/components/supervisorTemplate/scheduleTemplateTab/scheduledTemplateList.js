import React, { Component } from "react";
import MaterialTable from "material-table";
import tableIcons from "../../../styles/materialIcons";
import Select from 'react-select';
import SchedulerDialog from "./schedulerDialog";
import { addScheduledTemplates, deleteScheduledTemplates, getScheduledTemplates } from "../../../services/aws";
import { config } from "../../../../config/config";
import ConfirmDailog from "../dailogs/confirmAlert";
import "./schedule.css"
import WaitingModal from "../../Modals/WaitingModal/WaitingModal";
import moment from "moment";
import { connect } from "react-redux";
import { setUserTemplateMap } from '../../../actions/userActions'
import utils from "../../../utilities/utils";
export class sheduledTemplateTab extends Component {
    state = {
        data: [],
        open: false,
        currentRowData: [],
        openAlert: false,
        templateName: "",
        loader: false,
        oldData: [],
        waitModalHeader: ""
    }

    handleDialogClose = () => {
        this.setState({ open: false });
    }

    handleSaveData = async (dt) => {
        this.setState({ loader: true, open: false, waitModalHeader: `Updating the Schedule ${this.state.templateName}` });
        const toUpdate = this.props.userTemplateMap.filter(x => x.templateName === this.state.templateName)[0];
        toUpdate.scheduleDetails = dt;
        try {
            await addScheduledTemplates(
                `${config.backendEndpoint}template/schedules`,
                utils.accessToken(),
                utils.getEnv(),
                toUpdate
            );
            const scheduledtTemplate = await getScheduledTemplates();
            this.props.setUserTemplateMap(scheduledtTemplate.message)
            this.setState({ loader: false })
        } catch (err) {
            this.setState({ open: false, loader: false });
            console.log(err);
        }
    }

    deleteTemplates = async () => {
        this.setState({ loader: true, openAlert: false, waitModalHeader: `Deleting the Schedule ${this.state.templateName}` });
        try {
            await deleteScheduledTemplates(
                `${config.backendEndpoint}template/schedules?templateName=${this.state.templateName}&type=scheduleDelete`,
                utils.accessToken(),
                utils.getEnv()
            )
            const scheduledtTemplate = await this.getScheduledTemplate();
            this.props.setUserTemplateMap(scheduledtTemplate.message);
            this.setState({ loader: false })
        } catch (err) {
            this.setState({ open: false, loader: false })
            console.log(err);
        }
    }

    getScheduledTemplate = async () => {
        const scheduledTemplates = await getScheduledTemplates(
            `${config.backendEndpoint}template/schedules`,
            utils.accessToken(),
            utils.getEnv(),
        );
        return scheduledTemplates;
    }

    render() {
        return (
            <div className="userList" >
                <MaterialTable
                    title={"Scheduled Template"}
                    columns={[
                        { title: 'User Template', field: 'templateName' },
                        { title: 'Start Date', field: 'startDate' },
                        { title: 'End Date', field: 'endDate' },
                        { title: 'Schedule Type', field: 'cronExpression' },
                        { title: 'Latest Updated', field: 'lastAppliedDate' }
                    ]}
                    data={this.props.userTemplateMap.filter(x => (x.scheduleDetails !== undefined && Object.keys(x.scheduleDetails).length > 0)).map((x) => {
                        var newArray = {
                            templateName: x.templateName,
                            startDate: moment(x.scheduleDetails.startDate).format("YYYY-MM-DD"),
                            endDate: x.scheduleDetails.endDate ? moment(x.scheduleDetails.endDate).format("YYYY-MM-DD") : 'Never',
                            cronExpression: x.scheduleDetails.cronWordExp,
                            lastAppliedDate: (x.lastAppliedDate && x.lastAppliedDate !== "") ? `${moment(x.lastAppliedDate).utc().format("YYYY-MM-DD hh:mm:ss")} UTC` : "Queued for Execution"
                        }
                        return newArray
                    })}
                    icons={tableIcons}
                    actions={[
                        {
                            icon: tableIcons.Edit,
                            tooltip: 'Edit User Template',
                            onClick: (event, rowData) => {
                                this.setState({
                                    templateName: rowData.templateName,
                                    currentRowData: this.props.userTemplateMap.filter(x => x.templateName === rowData.templateName)[0].scheduleDetails,
                                    open: true
                                })
                            },
                        },
                        {
                            icon: tableIcons.Delete,
                            tooltip: 'Delete User Template',
                            onClick: (event, rowData) => {
                                this.setState({ templateName: rowData.templateName, openAlert: true })
                            }
                        }
                    ]}
                    options={{
                        search: true,
                        actionsColumnIndex: -1,
                        paging: false
                    }}
                />
                <WaitingModal isOpen={this.state.loader} header={this.state.waitModalHeader} />
                <SchedulerDialog dailogOpen={this.state.open} schedulerData={this.state.currentRowData} templateName={this.state.templateName} handleClose={this.handleDialogClose} toSaveData={this.handleSaveData} />
                <ConfirmDailog open={this.state.openAlert} title={"Confirmation"} confirm={this.deleteTemplates} subject={`Are you sure want to delete the Schedule for ${this.state.templateName}?`} handleClose={() => { this.setState({ openAlert: false }); }}></ConfirmDailog>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userTemplateMap: state.userTemplateMap
});

const mapDispatchToProps = (dispatch) => ({
    setUserTemplateMap: (userTemplateMap) => dispatch(setUserTemplateMap(userTemplateMap)),
});

export default connect(mapStateToProps, mapDispatchToProps)(sheduledTemplateTab);
