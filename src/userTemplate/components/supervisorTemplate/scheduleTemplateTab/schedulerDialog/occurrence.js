import React, { useEffect, useState } from "react"
import { Radio, TextField } from "@material-ui/core";
import DateTime from "./dateTimePicker";

const OccurrenceView = (props) => {
    const [endDateType, setEndDateType] = useState("");
    const [repeatOccurrence, setRepeatOccurrence] = useState(1);
    const [startDate, setStartDate] = useState(new Date())

    useEffect(() => {
        setStartDate(props.selectDate);
        if (props.repeatOcc) {
            setRepeatOccurrence(props.repeatOcc);
            setEndDateType(props.endDateType);
        }
    }, [props.selectDate])

    const handleEndDateChange = (e) => {
        setEndDateType(e.target.value)
        props.setEndtDateType(e.target.value)
    }

    const handleRepeatOccerence = (event) => {
        if (event.target.value >= 0 && event.target.value <= 31) {
            setRepeatOccurrence(event.target.value);
            props.repeatOccurrence(event.target.value)
        }
    }

    return (
        <div className="scheduler-textarea">
            <span className='font-size'>End Date</span>
            <div className="row">
                <div className="col-3">
                    <Radio
                        checked={endDateType === 'Never'}
                        onChange={handleEndDateChange}
                        value="Never"
                        name="radio-button-demo"
                        color={'primary'}
                    />
                    <label className="endDate-label">Never</label>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-3">
                    <Radio
                        checked={endDateType === 'On'}
                        onChange={handleEndDateChange}
                        value="On"
                        name="radio-button-demo"
                        color={'primary'}
                    />
                    <label className="endDate-label">On</label>
                </div>
                <div className="col-9">
                    <DateTime labelName={"End Date"} disable={endDateType !== 'On'} selectDate={startDate} selectedDate={(e) => { props.selectedDate(e) }} />
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-3">
                    <Radio
                        checked={endDateType === 'After'}
                        onChange={handleEndDateChange}
                        value="After"
                        color={'primary'}
                        name="radio-button-demo"
                    />
                    <label className="endDate-label">After</label>
                </div>
                <div className="col-3">
                    <TextField type="number" variant="outlined" label="Repeat" disabled={endDateType !== 'After'} fullWidth onChange={handleRepeatOccerence} value={repeatOccurrence} />
                </div>
                <div className="col-3">
                    <label className="endDate-label">Occurrence(s)</label>
                </div>
            </div>
        </div>
    )
}

export default OccurrenceView