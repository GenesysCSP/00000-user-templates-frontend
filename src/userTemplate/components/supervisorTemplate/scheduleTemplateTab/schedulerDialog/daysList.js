import React, { useEffect, useState } from 'react';
import { Button, ButtonGroup, TextField } from '@material-ui/core';
import '../schedule.css';

const DayListView = (props) => {
    const [repeatOn, setRepeatOn] = useState(1);
    const [sunClass, setSunClass] = useState('');
    const [monClass, setMonClass] = useState('');
    const [tueClass, setTueClass] = useState('');
    const [wedClass, setWedClass] = useState('');
    const [thuClass, setThuClass] = useState('');
    const [friClass, setFriClass] = useState('');
    const [satClass, setSatClass] = useState('');
    let [selectedDays] = useState([]);

    useEffect(() => {
        if (props.dayType === "Weekly" && props.sltDays) {
            selectedDays.length = 0;
            const days = props.sltDays.split(",")
            for (let i = 0; i < days.length; i++) {
                classChange(days[i]);
            }
        } else {
            setRepeatOn(props.sltDays ? props.sltDays : 1);
        }

    }, [])

    const handleRepeatOn = (event) => {
        if (event.target.value >= 0 && event.target.value <= 31) {
            setRepeatOn(event.target.value);
            props.selectedDays(event.target.value)
        }
    }

    const classChange = (e) => {
        const clas = !handleClassName(e) ? 'selected-days' : '';
        switch (e) {
            case 'Sun':
                setSunClass(clas);
                break;
            case 'Mon':
                setMonClass(clas);
                break;
            case 'Tue':
                setTueClass(clas);
                break;
            case 'Wed':
                setWedClass(clas);
                break;
            case 'Thu':
                setThuClass(clas);
                break;
            case 'Fri':
                setFriClass(clas);
                break;
            case 'Sat':
                setSatClass(clas);
                break;
        }
    }

    const handleClassName = (day) => {
        let isValid = true;
        const days = selectedDays.indexOf(day);
        if (days >= 0) {
            isValid = true;
            selectedDays.splice(days, 1)
        } else {
            selectedDays.push(day);
            isValid = false
        }
        return isValid;
    }

    const handleSelectDays = (e) => {
        classChange(e.target.outerText);
        props.selectedDays(selectedDays.toString());
    }

    return (
        <div className='dayTypes'>
            {props.dayType === "Weekly" ?
                <ButtonGroup variant="contained" aria-label="outlined primary button group">
                    <Button className={`capitalize ${sunClass}`} onClick={(e) => { handleSelectDays(e) }}>Sun</Button>
                    <Button className={`capitalize ${monClass}`} onClick={(e) => { handleSelectDays(e) }}>Mon</Button>
                    <Button className={`capitalize ${tueClass}`} onClick={(e) => { handleSelectDays(e) }}>Tue</Button>
                    <Button className={`capitalize ${wedClass}`} onClick={(e) => { handleSelectDays(e) }}>Wed</Button>
                    <Button className={`capitalize ${thuClass}`} onClick={(e) => { handleSelectDays(e) }}>Thu</Button>
                    <Button className={`capitalize ${friClass}`} onClick={(e) => { handleSelectDays(e) }}>Fri</Button>
                    <Button className={`capitalize ${satClass}`} onClick={(e) => { handleSelectDays(e) }}>Sat</Button>
                </ButtonGroup>
                :
                <TextField type="number" variant="outlined" fullWidth onChange={handleRepeatOn} value={repeatOn} />
            }
        </div>
    );
}

export default DayListView
