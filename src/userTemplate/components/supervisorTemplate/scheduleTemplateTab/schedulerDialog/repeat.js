import React, { useEffect, useState } from 'react';
import { InputLabel, Select, MenuItem, FormControl, TextField } from '@material-ui/core';
import '../schedule.css';
import DayListView from './daysList';

const RepeatView = (props) => {
    const [dayType, setDayType] = useState('Daily');
    const [repeatEvery, setRepeatEvery] = useState(1);
    const [selectedDay, setSelectedDays] = useState("")

    useEffect(() => {
        if (props.repeatEve || props.selectedDay) {
            setRepeatEvery(props.repeatEve);
            setDayType(props.selectedDay);
            setSelectedDays(props.sltdDays);
        }
    }, [props])

    const handleDayChange = (event) => {
        setDayType(event.target.value);
        setSelectedDays("");
        props.dayType(event.target.value);
    };

    const handleRepeatEvery = (event) => {
        if (event.target.value >= 0 && event.target.value <= 31) {
            setRepeatEvery(event.target.value);
            props.repeatEvery(event.target.value);
        }
    }

    const selectedDays = (e) => {
        props.selectedDays(e);
    }

    return (
        <div>
            <div className="row">
                <div className="col-8">
                    <FormControl fullWidth variant="outlined">
                        <InputLabel id="demo-simple-select-label">Repeat</InputLabel>
                        <Select
                            value={dayType}
                            label="Repeat"
                            onChange={handleDayChange}
                            MenuProps={{
                                anchorOrigin: {
                                    vertical: "bottom",
                                    horizontal: "left"
                                },
                                transformOrigin: {
                                    vertical: "top",
                                    horizontal: "left"
                                },
                                getContentAnchorEl: null
                            }}
                            className="selectDay"
                        >
                            <MenuItem value={"Daily"}>Daily</MenuItem>
                            <MenuItem value={"Weekly"}>Weekly</MenuItem>
                            <MenuItem value={"Monthly"}>Monthly</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                {(dayType !== "Weekly") &&
                    <div className="col-4">
                        <TextField type="number" variant="outlined" label="Repeat Every" fullWidth onChange={handleRepeatEvery} value={repeatEvery} />
                    </div>
                }

            </div>
            {(dayType !== "Daily") &&
                <div className="row daylist">
                    <div className="col-12">
                        <label className='font-size'>Repeat On</label>
                        <DayListView dayType={dayType} selectedDays={selectedDays} sltDays={selectedDay}></DayListView>
                    </div>
                </div>
            }
        </div>

    )
}

export default RepeatView;