import React, { useEffect, useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { DialogActions, TextField, Button, TextareaAutosize } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import '../schedule.css';
import DateTime from './dateTimePicker';
import Switch from '@material-ui/core/Switch';
import RepeatView from './repeat';
import OccurrenceView from './occurrence';
import { cronExpression } from '../cronExperssion';
import ConfirmDailog from '../../dailogs/confirmAlert';
import moment from 'moment';



const SchedulerDialog = (props) => {
    const [repeatOn, setRepeatOn] = useState(false);
    const [templateName, setTemplateName] = useState("");
    const [startDate, setStartDate] = useState(moment());
    const [endDateType, setEndtDateType] = useState("");
    const [repeatEvery, setRepeatEvery] = useState(1);
    const [repeatOccurrence, setRepeatOccurence] = useState(1);
    const [dayType, setDayType] = useState('Daily');
    const [selectedDays, setSelectedDays] = useState("");
    const [description, setDescription] = useState("");
    const [openAlert, setOpenAlert] = useState(false);
    const [endDateCalculation, setEndDateCalculation] = useState(moment())

    useEffect(() => {
        setTemplateName((props.templateName && typeof props.templateName === 'object') ? props.templateName.value : props.templateName);
        const isScheduleData = props.schedulerData;
        if (!isScheduleData) {
            return;
        }
        setStartDate(isScheduleData.startDate);
        setDescription(isScheduleData.description);
        if (isScheduleData.isRecursive) {
            setEndDateCalculation(isScheduleData.endDate);
            setRepeatOccurence(isScheduleData.repeatOccurrence);
            setRepeatEvery(isScheduleData.repeatEvery);
            setDayType(isScheduleData.repeat)
            setRepeatOn(isScheduleData.isRecursive);
            setSelectedDays(isScheduleData.selectedDay);
            setEndtDateType(isScheduleData.endDateType)
        } else {
            isInitialRender();
        }
    }, [props])

    const isInitialRender = () => {
        setEndDateCalculation(moment());
        setRepeatOccurence(1);
        setRepeatEvery(1);
        setDayType("Daily")
        setRepeatOn(false);
        setSelectedDays([]);
        setEndtDateType("Never")
    }

    const handleRepeatChange = (e) => {
        setRepeatOn(e.target.checked);
    }

    const handleClose = () => {
        props.handleClose();
        setRepeatOn(false)
    }

    const handleCronExp = () => {
        const cronExp = cronExpression(dayType, startDate, endDateCalculation, Number(repeatEvery), Number(repeatOccurrence), selectedDays, repeatOn, endDateType)
        if (repeatOn && endDateType === "Never") {
            cronExp.endDate = null;
        } else if (repeatOn && moment(cronExp.startDate).isAfter(moment(cronExp.endDate))) {
            return setOpenAlert(true);
        }
        cronExp.description = description;
        cronExp.endDateType = endDateType;
        props.toSaveData(cronExp);
    }

    return (
        <Dialog open={props.dailogOpen} disableEnforceFocus className='date-picker'>
            <DialogTitle>
                Scheduler
                <Close style={{ float: "right", cursor: "pointer" }} onClick={handleClose} />
            </DialogTitle>
            <DialogContent style={{ overflow: "hidden" }}>
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <TextField fullWidth id="outlined-basic" label="Template Name" value={templateName} disabled={true} variant="outlined" />
                        </div>
                    </div>
                    <div className="row scheduler-row">
                        <div className="col-7">
                            <DateTime labelName={"Schedule Date"} selectedDate={(e) => { setStartDate(e); setEndDateCalculation(e); }} startDate={startDate}></DateTime>
                        </div>
                        <div className="col-4 repeat">
                            <span className='ml-2 font-size '>Recursive</span>
                            <Switch
                                label="Repeat"
                                checked={repeatOn}
                                onChange={handleRepeatChange}
                                inputProps={{ 'aria-label': 'controlled' }}
                                color={'primary'}
                            />
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col scheduler-textarea">
                            <span className='font-size'>Description</span>
                            <TextareaAutosize
                                aria-label="minimum height"
                                minRows={3}
                                style={{ width: "100%" }}
                                value={description}
                                className="textarea"
                                onChange={(e) => { setDescription(e.target.value) }}
                            />
                        </div>
                    </div>
                </div>

                {repeatOn &&
                    <div className='container'>
                        <RepeatView dayType={(e) => { setDayType(e) }} selectedDays={(e) => { setSelectedDays(e) }} repeatEvery={(e) => { setRepeatEvery(e) }} repeatEve={repeatEvery} selectedDay={dayType} sltdDays={selectedDays}></RepeatView>
                        <OccurrenceView repeatOccurrence={(e) => { setRepeatOccurence(e) }} selectDate={endDateCalculation} selectedDate={(e) => { setEndDateCalculation(e); }} repeatOcc={repeatOccurrence} endDateType={endDateType} setEndtDateType={(e) => { setEndtDateType(e) }}></OccurrenceView>
                    </div>
                }
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={handleCronExp}>Save</Button>
            </DialogActions>
            <ConfirmDailog open={openAlert} title={"Warning"} confirm={() => { setOpenAlert(false) }} subject={'Please select the End date should be greater than Start date.'} handleClose={() => { setOpenAlert(false) }}></ConfirmDailog>
        </Dialog>
    )
}

export default SchedulerDialog;
