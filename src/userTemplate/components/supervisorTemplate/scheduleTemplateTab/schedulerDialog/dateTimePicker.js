import React, { useEffect, useState } from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import Calendar from "@material-ui/icons/CalendarTodayRounded";
import { IconButton, InputAdornment } from '@material-ui/core';

const DateTime = (props) => {
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [labelName, setLabelName] = useState("")

    useEffect(() => {
        setLabelName(props.labelName);
        if (props.selectDate) {
            setSelectedDate(props.selectDate)
        }
        if (props.startDate) {
            setSelectedDate(props.startDate);
        }
    }, [props.selectDate])

    const handleDateChange = (e) => {
        setSelectedDate(e);
        props.selectedDate(e);
    }

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DateTimePicker
                fullWidth
                value={selectedDate}
                disablePast
                onChange={handleDateChange}
                label={labelName}
                showTodayButton
                inputVariant="outlined"
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton>
                                <Calendar />
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
                disabled={props.disable}
                color={"primary"}
                format={labelName !== 'End Date' ? "yyyy/MM/dd hh:mm a" : "yyyy/MM/dd"}
            />
        </MuiPickersUtilsProvider>
    )
}

export default DateTime;