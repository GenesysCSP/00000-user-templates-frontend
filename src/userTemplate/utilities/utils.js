export const getParameterByName = (name) => {
  name = name.replace(/[[]/, "[").replace(/[\]]/, "\\]")
  const regex = new RegExp("[\\#&]" + name + "=([^&#]*)"),
    results = regex.exec(window.location.hash)
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
}

export const compareValues = (key, order = 'asc') => (a, b) => {
  if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
    // property doesn't exist on either object
    return 0;
  }

  const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
  const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

  let comparison = 0;
  if (varA > varB) {
    comparison = 1;
  } else if (varA < varB) {
    comparison = -1;
  }
  return order === 'desc' ? comparison * -1 : comparison;
};

export const isObjectEmpty = (obj) => {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};

const sleep = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const accessToken = () => {
  return sessionStorage.getItem('purecloud-csp-token') || sessionStorage.getItem(`purecloud-csp-token-${sessionStorage.getItem('orgId')}`)
}

const getEnv = () => {
  return sessionStorage.getItem('purecloud-csp-env') || sessionStorage.getItem(`purecloud-csp-env-${sessionStorage.getItem('orgId')}`)
}

export const calculateRequest = (queueLength, skillLength, addedUsers, removedUser, removeQueueLength) => {
  const getAll = addedUsers;
  const removeQueue = removeQueueLength * Math.ceil((addedUsers / 100));
  const removeSkill = removedUser + (removedUser * 2);
  const addQueue = queueLength * Math.ceil((addedUsers / 100));
  const addSkills = addedUsers * Math.ceil(skillLength / 20);

  const totalRequest = (getAll + removeQueue + removeSkill + addQueue + addSkills);
  return totalRequest;
}


function throttleApiRequests(cb, delay) {
  let wait = false;
  let storedArgs = [];

  function checkStoredArgs() {
    if (storedArgs?.length === 0) {
      wait = false;
    } else {
      cb(storedArgs[0]);
      storedArgs.splice(0, 1);
      setTimeout(checkStoredArgs, delay);
    }
  }

  return (args) => {
    if (wait) {
      storedArgs.push(args);
      return;
    }

    cb(args);
    wait = true;
    setTimeout(checkStoredArgs, delay);
  }
}

const chunksArray = (arr, chunkSize) => {
  const res = [];
  for (let i = 0; i < arr.length; i += chunkSize) {
    const chunk = arr.slice(i, i + chunkSize);
    res.push(chunk);
  }
  return res;
}

const handleSkillsList = (skills) => {
  let tempSkill = [], saveSkill = [], tempArraySize = 20;
  skills?.map(x => {
    let skillToAdd = {
      id: x.id,
      proficiency: x.proficiency
    };
    tempSkill.push(skillToAdd);
    if (tempSkill.length >= tempArraySize) {
      // tempArraySize = 10;
      saveSkill.push(tempSkill);
      tempSkill = [];
    }
  });

  if (tempSkill.length > 0)
    saveSkill.push(tempSkill);
  return saveSkill;
}

const differenceOf2Arrays = (array1, array2) => {
  const diff = [];
  for (let i = 0; i < array1.length; i++) {
    const found = array2.some((obj) => obj.name === array1[i].name);
    if (!found) {
      diff.push(array1[i]);
    }
  }
  return diff;
}

export default {
  sleep,
  compareValues,
  isObjectEmpty,
  getParameterByName,
  accessToken,
  getEnv,
  throttleApiRequests,
  chunksArray,
  calculateRequest,
  handleSkillsList,
  differenceOf2Arrays
}


