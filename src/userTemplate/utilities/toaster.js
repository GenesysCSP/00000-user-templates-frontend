import { IconButton, Snackbar } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import * as React from 'react';


const ToastMessage = (props) => {
  const handleClose = () => {
    props.handleClose();
  };

  const action = (
    <IconButton
      size="small"
      aria-label="close"
      color="inherit"
      onClick={handleClose}
    >
      <Close fontSize="small" />
    </IconButton>
  );

  return (
    <Snackbar
      open={props.open}
      autoHideDuration={10000}
      handleClose={handleClose}
      message={props.message}
      action={action}
      color="red"
    />
  );
}

export default ToastMessage;