import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import utils from '../utilities/utils'

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={(props) => (
      utils.accessToken()
        ? <Component {...props} />
        : <Redirect to='/login' />
    )} />
  )
}

export default PrivateRoute