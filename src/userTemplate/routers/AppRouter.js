import React from 'react'
import { Router, Switch } from 'react-router-dom'
import history from "./history";

import PublicRoute from './PublicRoute'
import PrivateRoute from './PrivateRoute'
import PureCloudLogin from '../components/login/PureCloudLogin'
import Home from '../components/home/Home'
import Help from '../components/Help/Help'
import Main from '../components/Main'
import ReleaseUpdate from '../components/ReleaseUpdates/releaseUpdate'

import Index from '../../wizard/components/IndexView'
import Install from '../../wizard/components/InstallView'
import Finish from '../../wizard/components/FinishView'

const AppRouter = () => (
    <Router history={history}>
        <Switch>
            <PublicRoute path="/" exact={true}  component={Index} history={history} />
            <PublicRoute path="/main" exact={true} component={Main} history={history} />
            <PublicRoute path="/login" exact={true} component={PureCloudLogin} />
            <PrivateRoute path="/dashboard" exact={true} component={Home} history={history} />
            <PublicRoute path="/help" exact={true} component={Help} history={history} />
            <PublicRoute path="/releaseUpdate" component={ReleaseUpdate} history={history} />

            <PublicRoute path="/index" exact={true} component={Index} history={history} />
            <PrivateRoute path="/install" exact={true} component={Install} history={history} />
            <PrivateRoute path="/finish/:errorMessage?" component={Finish} history={history} />
            <PublicRoute component={Index} history={history} />
            
        </Switch>
    </Router>
)

export default AppRouter