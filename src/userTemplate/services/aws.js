import axios from 'axios';
import promiseRetry from 'promise-retry';
const promiseRetryOptions = {
  retries: 1,
};

export const fetchApplications = async (configEndpoint, accessToken, environment) => {
  const result = {
    error: undefined,
    success: false,
    applications: [],
  };
  try {
    const options = {
      method: 'GET',
      url: configEndpoint,
      headers: {
        token: accessToken,
        env: environment,
        tokensource: 'purecloud'
      },
    };
    const response = await promiseRetry((retry, number) => {
      console.log('Fetching application list, attempt:', number);
      return sendRequest(options).catch((error) => {
        if (error.response && error.response.status === 401) {
          throw error;
        } else {
          retry(error);
        }
      });
    }, promiseRetryOptions);
    if (!response.success) {
      result.error = new Error(response.message);
    } else if (
      !response.message ||
      !Array.isArray(response.message.applications) ||
      response.message.applications.length === 0
    ) {
      result.error = new Error('No matching applications found');
    } else {
      result.applications = response.message.applications;
      result.success = true;
    }
    return result;
  } catch (error) {
    console.error('Error fetching applications: ', error);
    result.error = error;
    return result;
  }
};

export const getAuthorization = async (backendURL, token, env) => {
  const response = await fetch(`${backendURL}validateOrg`, {
    headers: {
      token,
      tokensource: 'purecloud',
      env
    }
  })
  const json = await response.json()
  return response.ok ? json : Promise.reject(json)
}

export const fetchUserTemplateMapping = (configEndpoint, token, env) => {
  return fetch(configEndpoint, {
    headers: {
      token,
      tokensource: 'purecloud',
      env
    }
  }).then(response => response.json())
}

export const setUserTemplateMapping = (setUserTemplateEndpoint, token, env, body) => {
  return fetch(setUserTemplateEndpoint, {
    method: "PUT",
    headers: {
      token,
      tokensource: 'purecloud',
      env
    },
    body: JSON.stringify(body)
  }).then(response => response.json()
  ).catch(error => console.log(error))
}

export const deleteUserTemplateMapping = (setUserTemplateEndpoint, token, env, body) => {
  return fetch(setUserTemplateEndpoint, {
    method: "POST",
    headers: {
      token,
      tokensource: 'purecloud',
      env
    },
    body: JSON.stringify(body)
  }).then(response => response.json()
  ).catch(error => console.log(error))
}

export const fetchApplicationConfig = (configEndpoint, accessToken, environment) => {
  return new Promise(async (resolve, reject) => {
    try {
      const result = {
        success: false,
        config: {},
      };
      const options = {
        method: 'GET',
        url: configEndpoint,
        headers: {
          token: accessToken,
          env: environment,
          //appId,
          tokensource: 'purecloud',
          'Accept': 'application/json'
        },
      };
      const response = await promiseRetry((retry, number) => {
        console.log('Fetching application configuration. Attempt:', number);
        return sendRequest(options).catch((error) => {
          if (error.response && error.response.status === 401) {
            throw error;
          } else {
            retry(error);
          }
        });
      }, promiseRetryOptions);
      if (!response.success) {
        const error = new Error(response.message);
        reject(error);
      } else {
        console.log('Configuration message was empty, but request was successful. This means the configuration exists, but has no values.');
        result.config = response.message;
        result.success = true;
        resolve(result);
      }
    } catch (error) {
      console.error('Error fetching application configuration: ', error);
      reject(error);
    }
  });
};

export const saveApplicationConfig = async (
  configEndpoint,
  accessToken,
  environment,
  configUpdate
) => {
  const result = {
    error: undefined,
    success: false,
  };
  try {
    const options = {
      method: 'PUT',
      url: configEndpoint,
      headers: {
        token: accessToken,
        env: environment,
        tokensource: 'purecloud',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      data: configUpdate
    };
    const response = await promiseRetry((retry, number) => {
      console.log('Saving application configuration. Attempt: ', number);
      return sendRequest(options).catch((error) => {
        console.log(error)
        if (error.response && error.response.status === 401) {
          throw error;
        } else {
          retry(error);
        }
      });
    }, promiseRetryOptions);
    console.log(response.success)
    if (!response.success) {
      result.error = new Error(response.message);
    } else {
      result.success = true;
    }
    return result;
  } catch (error) {
    console.error('Error saving application configuration: ', error);
    result.error = error;
    return result;
  }
};

const sendRequest = (options) => {
  return new Promise(async (resolve, reject) => {
    try {
      let result = {
        message: '',
        success: false,
      };
      const response = await axios(options);
      if (!response.data) {
        result.message = 'No data received from request';
      } else if (!response.data.success) {
        result.message = response.data.message;
      } else if (!response.data.message) {
        result.message = 'No message received from request';
      } else {
        result.message = response.data.message;
        result.success = true;
      }
      resolve(result);
    } catch (error) {
      console.error('Error processing sendRequest: ', error.response, error);
      reject(error);
    }
  });
};

export const getScheduledTemplates = (setUserTemplateEndpoint, token, env, body) => {
  return fetch(setUserTemplateEndpoint, {
    method: "GET",
    headers: {
      token,
      tokensource: 'purecloud',
      env
    },
    body: JSON.stringify(body)
  }).then(response => response.json()
  ).catch(error => console.log(error))
}

export const deleteScheduledTemplates = (setUserTemplateEndpoint, token, env, body) => {
  return fetch(setUserTemplateEndpoint, {
    method: "DELETE",
    headers: {
      token,
      tokensource: 'purecloud',
      env
    },
    body: JSON.stringify(body)
  }).then(response => response.json()
  ).catch(error => console.log(error))
}

export const addScheduledTemplates = (addUserTemplateEndpoint, token, env, body) => {
  return fetch(addUserTemplateEndpoint, {
    method: "POST",
    headers: {
      token,
      tokensource: 'purecloud',
      env
    },
    body: JSON.stringify(body)
  }).then(response => response.json()
  ).catch(error => console.log(error))
}

export const runTemplates = (runTemplateEndpoint, token, env) => {
  return fetch(runTemplateEndpoint, {
    method: "POST",
    headers: {
      token,
      tokensource: 'purecloud',
      env
    },
  }).then(response => response.json()
  ).catch(error => console.log(error))
}

export default {
  fetchApplications,
  getAuthorization,
  fetchUserTemplateMapping,
  setUserTemplateMapping,
  fetchApplicationConfig,
  saveApplicationConfig,
  getScheduledTemplates,
  deleteScheduledTemplates,
  addScheduledTemplates,
  runTemplates
};
