import promiseRetry from 'promise-retry';
import { config } from '../../config/config'
import utils from '../utilities/utils';
import utilities from '../utilities/utils';

const platformClient = window.require('platformClient');
const promiseRetryOptions = {
    retries: 2,
};

let routingApi;
let usersApi;

export const getEnvironments = () => {
    return config.regions
}

export const getClientId = (environment) => {
    return config.clientId
}

export const isLoggedIn = (token) => {
    return sessionStorage.getItem(token) !== null
}

export const initializeAPIs = () => {
    try {
        var client = platformClient.ApiClient.instance;
        client.setEnvironment(utils.getEnv());
        client.setAccessToken(utils.accessToken());
        routingApi = new platformClient.RoutingApi();
        usersApi = new platformClient.UsersApi();
    } catch (error) {
        console.error('Error initializing PureCloud APIs:', error);
    }
};

export const getOrgId = (token, environment) => {
    return fetch(`https://api.${environment}/api/v2/organizations/me`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
        .then(response => response.json())
        .catch(err => {
            console.log(err);
            return false
        })
}

export const getAuth = (token, environment) => {
    return fetch(`https://api.${environment}/api/v2/authorization/subjects/me`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

export const fetchRoles = (token, environment, page) => {
    return fetch(`https://api.${environment}/api/v2/authorization/roles?pageSize=${config.pageSize}&pageNumber=${page}&userCount=false`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

export const getAllRoles = (token, environment) => {
    return getRoles(token, environment)
}

const getRoles = (token, environment, page = 1, roles = []) => {
    return fetchRoles(token, environment, page)
        .then(response => {
            Array.prototype.push.apply(roles, response.entities)
            if (response.nextUri) {
                return getRoles(token, environment, response.pageNumber + 1, roles)
            } else {
                return roles
            }
        })
};

export const getMe = (token, environment) => {
    return fetch(`https://api.${environment}/api/v2/users/me?expand=authorization`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

export const fetchUsers = (token, environment, page) => {
    return fetch(`https://api.${environment}/api/v2/users?pageSize=${config.pageSize}&pageNumber=${page}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

export const getAllUsers = (token, environment) => {
    return getUsers(token, environment)
}

const getUsers = (token, environment, page = 1, users = []) => {
    return fetchUsers(token, environment, page)
        .then(response => {
            Array.prototype.push.apply(users, response.entities)
            if (response.nextUri) {
                return getUsers(token, environment, response.pageNumber + 1, users)
            } else {
                return users
            }
        })
};

export const fetchRoleUsers = (token, environment, page, roleId) => {
    return fetch(`https://api.${environment}/api/v2/authorization/roles/${roleId}/users?pageSize=${config.pageSize}&pageNumber=${page}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

export const getAllRoleUsers = (token, environment, roleId) => {
    return getRoleUsers(token, environment, roleId)
}

const getRoleUsers = (token, environment, roleId, page = 1, users = []) => {
    return fetchRoleUsers(token, environment, page, roleId)
        .then(response => {
            Array.prototype.push.apply(users, response.entities)
            if (response.pageCount > page) {
                return getRoleUsers(token, environment, roleId, response.pageNumber + 1, users)
            } else {
                return users
            }
        })
};

export const fetchSkills = () => {
    const entityType = 'skills';
    return fetchEntities(entityType);
};

export const fetchQueues = () => {
    const entityType = 'queues';
    return fetchEntities(entityType);
};

const fetchEntities = async (type, page) => {
    initializeAPIs();
    let result = {
        success: false,
        entities: [],
        error: '',
    };
    try {
        let pageNumber = page || 1;
        const options = {
            pageSize: config.pageSize,
            pageNumber,
        };

        let getEntities;
        const initialEntities = await promiseRetry((retry, number) => {
            console.log(`Fetching PureCloud ${type}. Page: ${pageNumber}, Attempt: ${number}`);
            if (type === 'users') {
                getEntities = usersApi.getUsers(options);
            } else if (type === 'queues') {
                getEntities = routingApi.getRoutingQueues(options);
            } else if (type === 'skills') {
                getEntities = routingApi.getRoutingSkills(options);
            }
            return getEntities.catch((error) => {
                if (error.status === 401) {
                    throw error;
                } else {
                    retry(error);
                }
            });
        }, promiseRetryOptions);

        let remainingPages = [];
        if (pageNumber < initialEntities.pageCount) {
            remainingPages = await promiseRetry((retry, number) => {
                console.log(
                    `Fetching remaining PureCloud ${type}. ` +
                    `Total pages: ${initialEntities.pageCount}, Attempt: ${number}`
                );
                const getRemainingEntities = [];
                pageNumber += 1;
                while (pageNumber <= initialEntities.pageCount) {
                    options.pageNumber = pageNumber;
                    if (type === 'users') {
                        getEntities = usersApi.getUsers(options);
                    } else if (type === 'queues') {
                        getEntities = routingApi.getRoutingQueues(options);
                    } else if (type === 'skills') {
                        getEntities = routingApi.getRoutingSkills(options);
                    }
                    getRemainingEntities.push(getEntities);
                    pageNumber += 1;
                }
                return Promise.all(getRemainingEntities).catch((error) => {
                    if (error.status === 401) {
                        throw error;
                    } else {
                        retry(error);
                    }
                });
            }, promiseRetryOptions);
        }

        let entities = initialEntities.entities;
        if (remainingPages.length > 0) {
            remainingPages.forEach((remainingPage) => {
                entities = entities.concat(remainingPage.entities);
            });
        }

        result.entities = entities.map((entity) => {
            const item = {
                id: entity.id,
                name: entity.name,
            };
            if (type === 'users') {
                item.username = entity.username;
            } else if (type === 'queues') {
                item.description = entity.description;
            } else if (type === 'skills') {
                item.description = entity.description;
            }
            return item;
        });
        result.success = true;
        return result;
    } catch (error) {
        if (error.status === 401) {
            console.warn('User is not authenticated to PureCloud');
        } else {
            console.error(`Error fetching PureCloud ${type}:`, error);
        }
        result.error = error;
        return result;
    }
};

export const setUserSkills = (token, env, userId, skills, requestType) => {
    console.log("Setting skills for User" + userId)
    return fetch(`https://api.${env}/api/v2/users/${userId}/routingskills/bulk`, {
        method: requestType,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(skills)
    }).then(response => response.json())
}

export const removeUserSkills = (token, env, userId, skillId) => {
    console.log("Removing user skill " + skillId)
    return fetch(`https://api.${env}/api/v2/users/${userId}/routingskills/${skillId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

const getAllUserQueues = async (token, env, userId) => {
    let allQueuesRetrieved = false
    let uri = `/api/v2/users/${userId}/queues?pageSize=${config.pageSize}&pageNumber=1`
    let allUserQueues = []
    while (!allQueuesRetrieved) {
        const queuesList = await getUserQueues(env, token, uri)
        allUserQueues = allUserQueues.concat(queuesList.entities)
        queuesList.nextUri ? uri = queuesList.nextUri : allQueuesRetrieved = true
    }
    return allUserQueues
}

const getUserQueues = (env, token, uri) => {
    return fetch(`https://api.${env}${uri}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    }).then(response => response.json())
}

const addRemoveUserQueues = (token, env, users, queueId, isDelete) => {
    console.log(`Setting queue for users : ${isDelete}` + JSON.stringify(users))
    return fetch(`https://api.${env}/api/v2/routing/queues/${queueId}/members?delete=${isDelete}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        },
        body: JSON.stringify(users)
    }).then(response => response)
}

export const getAllQueues = async (users) => {
    let allQueues = [];
    for (let index = 0; index < users?.length; index++) {
        const queueList = await getAllUserQueues(utils.accessToken(), utils.getEnv(), users[index]);
        queueList.map(x => {
            if (x) {
                if (!allQueues.includes(x.id)) {
                    allQueues.push(x.id)
                }
            }
        })
    }
    return allQueues;
}

export const addRemoveQueues = async (users, isDelete, queues) => {
    let failedResponse = true;
    for (let u = 0; u < users?.length; u++) {
        const tempIds = [], userIds = [];
        users[u]?.map(x => tempIds.push({ "id": x }));
        userIds.push(tempIds);
        for (let i = 0; i < queues?.length; i++) {
            var susResponse = { status: 200 };
            susResponse = await addRemoveUserQueues(utils.accessToken(), utils.getEnv(), userIds[0], queues[i], isDelete);
            if (susResponse.status) {
                if (susResponse.status === 200)
                    failedResponse = false;
                else if (susResponse.status === 429) {
                    await utilities.sleep(250)
                } else if (susResponse.status === 504) {
                    await utilities.sleep(7000)
                }
                else
                    throw new Error({ status: "500", message: "Failed in removal of skills" })
            } else {
                failedResponse = false
            }
        }
    }
}

export const removeSkills = async (users, skill) => {
    let failedResponse = true;
    for (let i = 0; i < users?.length; i++) {
        do {
            try {
                var susResponse = { status: 200 }, rusResponse = { status: 200 };
                if (skill) {
                    let skillToRemove = {
                        id: skill.id,
                        proficiency: skill.proficiency
                    }

                    if (skill) {
                        susResponse = await setUserSkills(utils.accessToken(), utils.getEnv(), users[i], [skillToRemove], 'PUT')
                    } else {
                        susResponse = { status: 200 }
                    }

                    rusResponse = await removeUserSkills(utils.accessToken(), utils.getEnv(), users[i], skillToRemove.id)
                }
                if (susResponse.status && rusResponse.status) {
                    if (susResponse.status === 200 && rusResponse.status === 200)
                        failedResponse = false
                    else if (susResponse.status === 429 || rusResponse.status === 429) {
                        await utilities.sleep(250)
                    }
                    else if (susResponse.status === 504 || rusResponse.status === 504) {
                        //Adding skill needs time gap between requests
                        await utilities.sleep(7000)
                    }
                    else
                        throw new Error({ status: "500", message: "Failed in removal of skills" })
                } else {
                    failedResponse = false
                }

            } catch (error) {
                console.log('error in removing skills:', JSON.stringify(error))
                if (error.status === 429) {
                    console.log(`Waiting ${error.retryAfterMs / 1000} seconds before reaching to Genesys Cloud`)
                    await utilities.sleep(error.retryAfterMs)
                } else throw error
            }
        } while (failedResponse)
    }
    return true;
}

export const addSkills = async (users, skillList, typeRequest) => {
    for (let s = 0; s < skillList.length; s++) {
        for (let i = 0; i < users?.length; i++) {
            let failedResponse = true
            do {
                try {
                    if (skillList.length > 0) {
                        let ausResponse
                        ausResponse = await setUserSkills(utils.accessToken(), utils.getEnv(), users[i], skillList[s], typeRequest)
                        if (ausResponse.status) {
                            if (ausResponse.status === 200) {
                                failedResponse = false;
                            } else if (ausResponse.status === 429) {
                                console.log(`Waiting ${ausResponse.retryAfterMs / 1000} seconds before reaching to Genesys Cloud`)
                                await utilities.sleep(200)
                            } else if (ausResponse.status === 504) {
                                await utilities.sleep(7000)
                            } else {
                                throw new Error({ status: ausResponse.status, message: ausResponse.message })
                            }
                        } else {
                            failedResponse = false;
                        }
                    }
                } catch (error) {
                    console.log('error in adding skills:', JSON.stringify(error))
                    if (error.status === 429) {
                        console.log(`Waiting ${error.retryAfterMs / 1000} seconds before reaching to Genesys Cloud`)
                        await utilities.sleep(error.retryAfterMs)
                    } else throw error
                }
            } while (failedResponse)
        }
    }
    return true;
}

export const getOrgConfig = (configEndpoint, token, env) => {
    return fetch(configEndpoint, {
        headers: {
            token,
            tokensource: 'purecloud',
            env
        }
    }).then(response => response.json())
}

