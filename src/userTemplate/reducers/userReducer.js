const defaultUserState = {
    userAuths: [],
    department: '',
    userName: '',
    userId: '',
    orgId: '',
    templates: [],
    filteredTemplates: [],
    isFiltered: false,
    allUsers: [],
    userTemplateMap: [],
    isPureCloud: false,
    assignedTemplate: 'No Template Assigned',
    assignedQueue: 'No Queue',
    assignedSkill: 'No Skill',
    templateRole: '',
    allQueues: [],
    allSkills: [],
    updatesPending: false,
    isUpdating: false
}

const userReducer = (state = defaultUserState, action) => {
    switch (action.type) {
        case 'SET_USER_AUTH':
            return {
                ...state,
                userAuths: action.userAuths
            }
        case 'SET_ORG_ID':
            return {
                ...state,
                orgId: action.orgId
            }
        case 'SET_USER_PROPS':
            return {
                ...state,
                userName: action.userName,
                userId: action.userId,
                department: action.department
            }
        case 'SET_TEMPLATES':
            return {
                ...state,
                templates: action.templates.map((template) => ({
                    value: template.displayName,
                    label: template.displayName,
                    ...template
                }))
            }
        case 'DELETE_TEMPLATE':
            return {
                ...state,
                templates: state.templates.filter(s => s.displayName !== action.template)
            }
        case 'SET_PURECLOUD':
            return {
                ...state,
                isPureCloud: action.isPureCloud
            }
        case 'SET_USERS':
            return {
                ...state,
                allUsers: action.allUsers
            }
        case 'SET_TEMPLATE_MAP':
            return {
                ...state,
                userTemplateMap: action.userTemplateMap.filter(x => x)
            }
        case 'SET_ASSIGNED_TEMPLATE':
            return {
                ...state,
                assignedTemplate: action.assignedTemplate
            }
        case 'SET_ASSIGNED_QUEUE':
            return {
                ...state,
                assignedQueue: action.assignedQueue
            }
        case 'SET_ASSIGNED_SKILL':
            return {
                ...state,
                assignedSkill: action.assignedSkill
            }
        case 'SET_FILTERED_TEMPLATES':
            return {
                ...state,
                filteredTemplates: action.filteredTemplates.map((template) => ({
                    value: template.displayName,
                    label: template.displayName,
                    ...template
                }))
            }
        case 'SET_IS_FILTERED':
            return {
                ...state,
                isFiltered: action.isFiltered
            }
        case 'SET_IS_UPDATING':
            return {
                ...state,
                isUpdating: action.isUpdating
            }
        case 'SET_TEMPLATE_ROLE':
            return {
                ...state,
                templateRole: action.templateRole
            }
        case 'SET_QUEUES':
            return {
                ...state,
                allQueues: action.queues
            }
        case 'SET_SKILLS':
            return {
                ...state,
                allSkills: action.skills.map((skill) => ({
                    value: skill.name,
                    label: skill.name,
                    proficiency: "0",
                    ...skill
                }))
            }
        case 'SET_UPDATES_PENDING':
            return {
                ...state,
                updatesPending: action.pending
            }
        default:
            return state
    }
}

export default userReducer