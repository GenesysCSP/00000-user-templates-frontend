const envVar = process.env.REACT_APP_CUSTOM_ENV.trim()
const env = envVar === "production" ? "" : envVar === 'test' ? " Test" : envVar === 'demo' ? ' Demo' : " Dev";
console.log("env: ", env)
export const appName = "User Templates" + env;
export const userRole = appName + " User";
export const agentRole = appName + " Agent";
export const supervisorRole = appName + " Supervisor";

const provisioningInfo = {
    roles: [
        {
            name: appName,
            description: 'Generated role that is used by the application backend',
            permissionPolicies: [
                { // permission to allow backend to retrieve all integration types in validate org
                    domain: 'integrations',
                    entityName: 'integration',
                    actionSet: ["view"],
                    allowConditions: false
                },
                {
                    "domain": "directory",
                    "entityName": "userProfile",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "user",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "routing",
                    "entityName": "language",
                    "actionSet": ["assign"],
                    "allowConditions": false
                },
                {
                    "domain": "routing",
                    "entityName": "skill",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "routing",
                    "entityName": "queue",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "userProfile",
                    "actionSet": ["edit"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "user",
                    "actionSet": ["edit"],
                    "allowConditions": false
                },
                {
                    "domain": "integration",
                    "entityName": "userTemplates",
                    "actionSet": ["view"],
                    "allowConditions": false
                }]
        },
        {
            name: userRole,
            description: 'Generated role for User access',
            permissionPolicies: [
                { // permission to allow backend to retrieve all integration types in validate org
                    domain: 'integrations',
                    entityName: 'integration',
                    actionSet: ["view"],
                    allowConditions: false
                },
                {
                    "domain": "directory",
                    "entityName": "userProfile",
                    "actionSet": ["view"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "user",
                    "actionSet": ["view"],
                    "allowConditions": false
                },
                {
                    "domain": "integration",
                    "entityName": "userTemplates",
                    "actionSet": ["*"],
                    "allowConditions": false
                }]
        },
        {
            name: agentRole,
            description: 'Generated role for User access',
            permissionPolicies: [
                { // permission to allow backend to retrieve all integration types in validate org
                    domain: 'integrations',
                    entityName: 'integration',
                    actionSet: ["view"],
                    allowConditions: false
                },
                {
                    "domain": "directory",
                    "entityName": "userProfile",
                    "actionSet": ["edit"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "user",
                    "actionSet": ["edit"],
                    "allowConditions": false
                },
                {
                    "domain": "integration",
                    "entityName": "userTemplates",
                    "actionSet": ["*"],
                    "allowConditions": false
                }]
        },
        {
            name: supervisorRole,
            description: 'Generated role for Supervisor access',
            permissionPolicies: [
                { // permission to allow backend to retrieve all integration types in validate org
                    domain: 'integrations',
                    entityName: 'integration',
                    actionSet: ["view"],
                    allowConditions: false
                },
                {
                    "domain": "authorization",
                    "entityName": "role",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "routing",
                    "entityName": "language",
                    "actionSet": ["assign"],
                    "allowConditions": false
                },
                {
                    "domain": "routing",
                    "entityName": "skill",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "routing",
                    "entityName": "queue",
                    "actionSet": ["*"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "userProfile",
                    "actionSet": ["edit"],
                    "allowConditions": false
                },
                {
                    "domain": "directory",
                    "entityName": "user",
                    "actionSet": ["edit"],
                    "allowConditions": false
                },
                {
                    "domain": "integration",
                    "entityName": "userTemplates",
                    "actionSet": ["*"],
                    "allowConditions": false
                }]
        }
    ],
    oauth: {
        name: appName + ' OAuth',
        description: `Generated OAuth Client that is used by the application backend`,
        authorizedGrantType: 'CLIENT_CREDENTIALS'
    }
};

const development = {
    //backendEndpoint: 'http://localhost:6000/dev/', // backend endpoint
    backendEndpoint: 'https://6w1ryq0jb1.execute-api.us-east-1.amazonaws.com/dev/', // backend endpoint
    clientId: '62f4ca4e-d546-4f1c-817b-c5f0bb39f8c3',
    // clientId: 'd4dfa94d-d68a-417f-800c-baf332b178c1',
    appName: appName,
    // appUrl: "http://localhost:3000/",
    appUrl: "https://d2j3bauhobljzt.cloudfront.net/",
    integrationType: "premium-app-example",
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",
    regions: ['mypurecloud.com', 'mypurecloud.com.au', 'mypurecloud.jp', 'mypurecloud.ie', 'mypurecloud.de', 'usw2.pure.cloud', 'use2.us-gov-pure.cloud', 'cac1.pure.cloud', 'sae1.pure.cloud', 'mypurecloud.de', 'mypurecloud.ie', 'euw2.pure.cloud', 'aps1.pure.cloud', 'apne2.pure.cloud'],
    provisioningInfo: provisioningInfo,
    permissionPolicyEntityName: "examplePremiumApp",
    pageSize: 200
}

const test = {
    backendEndpoint: 'https://6w1ryq0jb1.execute-api.us-east-1.amazonaws.com/test/', // backend endpoint
    clientId: '62f4ca4e-d546-4f1c-817b-c5f0bb39f8c3',
    // clientId: 'd4dfa94d-d68a-417f-800c-baf332b178c1',
    appName: appName,
    // appUrl: "http://localhost:3000/",
    appUrl: "https://d2j3bauhobljzt.cloudfront.net/",
    integrationType: "premium-app-example",
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",
    regions: ['mypurecloud.com', 'mypurecloud.com.au', 'mypurecloud.jp', 'mypurecloud.ie', 'mypurecloud.de', 'usw2.pure.cloud', 'use2.us-gov-pure.cloud', 'cac1.pure.cloud', 'sae1.pure.cloud', 'mypurecloud.de', 'mypurecloud.ie', 'euw2.pure.cloud', 'aps1.pure.cloud', 'apne2.pure.cloud'],
    provisioningInfo: provisioningInfo,
    permissionPolicyEntityName: "examplePremiumApp",
    pageSize: 200
}

const production = {
    backendEndpoint: 'https://c6dzb86weh.execute-api.us-east-1.amazonaws.com/prod/', // backend endpoint
    clientId: '62f4ca4e-d546-4f1c-817b-c5f0bb39f8c3',
    appName: appName,
    appUrl: "https://usertemplates.genesyscsp.com/",
    integrationType: "premium-app-usertemplates",
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",
    regions: ['mypurecloud.com', 'mypurecloud.com.au', 'mypurecloud.jp', 'mypurecloud.ie', 'mypurecloud.de', 'usw2.pure.cloud', 'use2.us-gov-pure.cloud', 'cac1.pure.cloud', 'sae1.pure.cloud', 'mypurecloud.de', 'mypurecloud.ie', 'euw2.pure.cloud', 'aps1.pure.cloud', 'apne2.pure.cloud'],
    provisioningInfo: provisioningInfo,
    permissionPolicyEntityName: "userTemplates",
    pageSize: 200
}

const demo = {
    backendEndpoint: 'https://n2w9qm55w1.execute-api.us-east-1.amazonaws.com/demo/', // backend endpoint
    clientId: '62f4ca4e-d546-4f1c-817b-c5f0bb39f8c3',
    appName: appName,
    appUrl: "https://d262nywldynsrj.cloudfront.net/",
    integrationType: "premium-app-example",
    defaultPcEnv: "mypurecloud.com",
    defaultLangTag: "en-us",
    regions: ['mypurecloud.com', 'mypurecloud.com.au', 'mypurecloud.jp', 'mypurecloud.ie', 'mypurecloud.de', 'usw2.pure.cloud', 'use2.us-gov-pure.cloud'],
    provisioningInfo: provisioningInfo,
    permissionPolicyEntityName: "examplePremiumApp",
    pageSize: 200
}

export const config = envVar === "production" ? production : envVar === "development" ? development : envVar === "demo" ? demo : test;

export const headerTitle = "User Templates";

