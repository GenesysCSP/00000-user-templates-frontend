import React, { Component } from 'react';
import '../style.css';
import WizardApp from '../scripts/wizard-app';

let userTemplateApp;

class InstalView extends Component {
     componentDidMount() {
        userTemplateApp = new WizardApp();
        userTemplateApp.start();
    };

    InstallConfiguration = () => {

        userTemplateApp.installConfigurations().then(() => {
            this.props.history.push('/finish');
            // setTimeout(() => {
            //     //goToPage('./finish', err);
            //     this.props.history.push('/finish');
            // }, 2000);  
        }).catch((err) => {
            this.props.history.push('/finish/' + err);
            // setTimeout(() => {
            //     //goToPage('./finish', err);
            //     this.props.history.push('/finish');
            // }, 2000);                        
        });
    };

    render() {
        return (
            <div className="full-height">
                <div id="background"></div>
                <noscript>
                    For full functionality of this site it is necessary to enable JavaScript. Here are the <a
                        href="http://www.enable-javascript.com/" target="_blank" rel="noopener noreferrer">instructions how to enable JavaScript in your web
                    browser</a>.
                    </noscript>

                <header>
                    <p>User Templates /&nbsp; Install</p>
                </header>
                <div className="wiz-content">
                    <div className="title">
                        Installation
                        </div>

                    <ul className="wiz-progress-bar">
                        <li className="active">
                            <span className="txt-start">Start</span>
                        </li>
                        <li className="active current">
                            <span className="txt-install">Install</span>
                        </li>
                        <li className="">
                            <span className="txt-summary">Summary</span>
                        </li>
                    </ul>

                    <main>
                        <p>
                            <span className="txt-install-summary">These are the steps that will be automatically performed for
                                    you!</span>
                        </p>

                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-role">1. Create Roles</span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-role-msg">
                                        Create roles specifically to provide access to the app.
                                        </span>
                                </div>
                            </div>
                        </div>

                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-oauthclient">
                                    2. Create App Instance
                                        </span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-oauthclient-msg">
                                        Create OAuth Client for server-to-server communication.
                                        </span>
                                </div>
                            </div>
                        </div>

                        <div className="message">
                            <div className="message-title">
                                <span className="txt-create-instance">
                                    3. Configure instance
                                    </span>
                            </div>
                            <div className="message-content">
                                <div>
                                    <span className="txt-create-instance-msg">
                                        Configure app instance URI endpoint.
                                        </span>
                                </div>
                            </div>
                        </div>


                        <p className="sink-more">
                            <span className="txt-start-install">
                                Please click the button to start the installation.
                                </span>
                        </p>

                        <button id="start" onClick={this.InstallConfiguration.bind(this)} className="wiz-btn-info right">
                            <span className="txt-install">Install</span>
                        </button>
                    </main>
                </div>
                <footer>
                </footer>
            </div>
        )
    }
};

export default InstalView;