## User Template
## App for installation of the User Template

## Features

- The Application is Integrated with Genesys Cloud
- Following are the feature of the application
    > Create Template
    > Edit Template
    > Apply Template
    > Scheduled Template
    > Agent View
    > User View
- We can map specific queues and skills to a particular template and assign the template to a group of agents.
- We can assign the template to a specific queue with skills and assign it to Agent.
- Templates can also be scheduled to run at a specific time. The schedule can be both recursive and non-recursive. The template will be assigned to the agents at the scheduled time.
- The Agent View will list all the available templates. On selecting a template from the drop-down, the Queues and Skills mapped to the template will be shown. The agent users should be assigned the User Template User and User Template Agent roles.
- User View, The page shows the details of the template assigned to the user.

The web application is developed in ReactJS, deployed in s3, and accessed via CloudFront. The application internally calls the Genesys API with a custom wrapper to handle multiple requests

> The app is available in app foundry
> The app is added to each org via app foundry 
> The installation of the app is done through the installation wizard after the integration

## Installation

The installation is done via the Wizard installation. It’s a one-time process for each organization. A user with installation permission(created while enabling the app from the app foundry) will be able to complete the installation. The installation is a 3 step process; the user can choose the type of product during the installation.

## URL

> dev Url -> https://d1ziuqn7xcwka1.cloudfront.net/
> test Url -> https://d2j3bauhobljzt.cloudfront.net/
> production Url -> https://usertemplates.genesyscsp.com/
> demo Url -> https://d262nywldynsrj.cloudfront.net/
