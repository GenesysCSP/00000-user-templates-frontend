const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = (env) => {
    return {
        mode: 'development',
        entry: "/src/index.js",
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: 'usertemplate.bundle.[contenthash].js',
            clean: true
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env", "@babel/preset-react"],
                        },
                    },
                },
                {
                    test: /\.(css|s[ac]ss)$/i,
                    use: ["style-loader", "css-loader", "sass-loader",],
                },
                {
                    test: /\.html$/,
                    exclude: [/node_modules/, require.resolve('./public/userTemplateIndex.html')],
                    use: {
                        loader: 'file-loader'
                    },
                }
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './public/userTemplateIndex.html',
                title: 'Caching',
            }),
            new webpack.DefinePlugin({
                process: {
                    env
                }
            })
        ]
    }
};
